﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using MomentumPlus.Relay.Authorization.Auth.Cookie;
using MomentumPlus.Relay.Authorization.Auth.Managers;
using MomentumPlus.Relay.Authorization.Domain;

namespace MomentumPlus.Relay.Authorization.Auth
{
    public static class AuthModuleConfiguration
    {
        public static void Apply(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(MultiTenancyContext.Create);
            app.CreatePerOwinContext<RelayUserManager>(RelayUserManager.Create);
            app.CreatePerOwinContext<RelaySignInManager>(RelaySignInManager.Create);
            app.CreatePerOwinContext<RelayRoleManager>(RelayRoleManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Auth/Account/Login"),
                Provider = CookieProvider.Create(),
                SlidingExpiration = true,
                ExpireTimeSpan = TimeSpan.FromMinutes(32)
            });
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);
        }
    }
}
