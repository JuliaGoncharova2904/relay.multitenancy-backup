﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using System;
using System.Web;
using MomentumPlus.Relay.Authorization.Auth.Managers;
using MomentumPlus.Relay.Authorization.Domain.Entities;

namespace MomentumPlus.Relay.Authorization.Auth.Cookie
{
    public static class CookieProvider
    {
        public static CookieAuthenticationProvider Create()
        {
            CookieAuthenticationProvider provider = new CookieAuthenticationProvider();

            var originalHandler = provider.OnApplyRedirect;

            provider.OnApplyRedirect = context =>
            {
                //if (!IsApiRequest(context.Request) || !IsAjaxRequest(context.Request))
                //{
                //    context.Response.Redirect(context.RedirectUri);
                //}

                if (!IsAjaxRequest(context.Request))
                {
                    context.Response.Redirect(context.RedirectUri);
                }

                //string NewURI = "/Auth/Account";
                ////Overwrite the redirection uri
                //context.RedirectUri = NewURI;
                //originalHandler.Invoke(context);

            };

            provider.OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<RelayUserManager, RelayUser, Guid>(
                validateInterval: TimeSpan.FromMinutes(30),
                regenerateIdentityCallback: (manager, user) => user.GenerateUserIdentityAsync(manager),
                getUserIdCallback: user => Guid.Parse(user.GetUserId()));


            return provider;
        }

        private static bool IsApiRequest(IOwinRequest request)
        {
            string apiPath = VirtualPathUtility.ToAbsolute("~/api/");
            return request.Uri.LocalPath.ToLower().StartsWith(apiPath);
        }

        private static bool IsAjaxRequest(IOwinRequest request)
        {
            IReadableStringCollection queryXML = request.Query;
            if ((queryXML != null) && (queryXML["X-Requested-With"] == "XMLHttpRequest"))
            {
                return true;
            }

            IReadableStringCollection queryJSON = request.Query;
            if ((queryJSON != null) && (queryJSON["Content-Type"] == "application/json"))
            {
                return true;
            }

            IHeaderDictionary headersXML = request.Headers;
            var isAjax = ((headersXML != null) && (headersXML["X-Requested-With"] == "XMLHttpRequest"));

            IHeaderDictionary headers = request.Headers;
            var isJson = ((headers != null) && (headers["Content-Type"] == "application/json"));

            return isAjax || isJson;

        }
    }
}
