﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using MomentumPlus.Relay.Authorization.Domain;
using MomentumPlus.Relay.Authorization.Domain.Entities;

namespace MomentumPlus.Relay.Authorization.Auth.Managers
{
    public class RelayRoleManager : RoleManager<RelayRole,Guid>
    {
        public RelayRoleManager(
            IRoleStore<RelayRole, Guid> roleStore)
            : base(roleStore)
        {
        }
        public static RelayRoleManager Create(
            IdentityFactoryOptions<RelayRoleManager> options, IOwinContext context)
        {
            return new RelayRoleManager(
                new RoleStore<RelayRole, Guid, RelayUserRole>(context.Get<MultiTenancyContext>()));
        }
    }
}
