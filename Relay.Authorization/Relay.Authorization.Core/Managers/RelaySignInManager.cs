﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using MomentumPlus.Relay.Authorization.Domain.Entities;

namespace MomentumPlus.Relay.Authorization.Auth.Managers
{
    // Configure the application sign-in manager which is used in this application.
    public class RelaySignInManager : SignInManager<RelayUser, Guid>
    {
        public RelaySignInManager(RelayUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(RelayUser user)
        {
            return user.GenerateUserIdentityAsync((RelayUserManager)UserManager);
        }

        public static RelaySignInManager Create(IdentityFactoryOptions<RelaySignInManager> options, IOwinContext context)
        {
            return new RelaySignInManager(context.GetUserManager<RelayUserManager>(), context.Authentication);
        }
    }
}
