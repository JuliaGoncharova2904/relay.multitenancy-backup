﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using MomentumPlus.Relay.Authorization.Auth.Services;
using MomentumPlus.Relay.Authorization.Domain;
using MomentumPlus.Relay.Authorization.Domain.Entities;

namespace MomentumPlus.Relay.Authorization.Auth.Managers
{
    // Configure the application user manager used in this application.UserManager is defined in ASP.NET Identity and is used by the application.
    public class RelayUserManager : UserManager<RelayUser, Guid>
    {
        public RelayUserManager(IUserStore<RelayUser, Guid> store)
            : base(store)
        {
        }

        public static RelayUserManager Create(IdentityFactoryOptions<RelayUserManager> options, IOwinContext context)
        {
            var manager = new RelayUserManager(new UserStore<RelayUser, RelayRole, Guid, RelayUserLogin, RelayUserRole, RelayUserClaim>(context.Get<MultiTenancyContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<RelayUser, Guid>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<RelayUser, Guid>
            {
                MessageFormat = "Your security code is {0}"
            });
            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<RelayUser, Guid>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<RelayUser, Guid>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }
}
