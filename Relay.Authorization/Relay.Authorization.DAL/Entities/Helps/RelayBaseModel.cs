﻿using System;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public abstract class RelayBaseModel
    {
        public Guid Id { get; set; }

        public DateTime? CreatedUtc { get; set; }

        public DateTime? ModifiedUtc { get; set; }

        public DateTime? DeletedUtc { get; set; }
    }
}
