﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class RelayLogoFile
    {
        public Guid Id { get; set; }
        public byte[] BinaryData { get; set; }
        public string FileType { get; set; }
        public string ContentType { get; set; }
        public Guid? RotationId { get; set; }
    }
}
