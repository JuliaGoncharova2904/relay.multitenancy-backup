﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class RelayRotation
    {
        public Guid Id { get; set; }

        public DateTime? CreatedUtc { get; set; }

        public DateTime? ModifiedUtc { get; set; }

        public DateTime? DeletedUtc { get; set; }

        public Guid RotationOwnerId { get; set; }

        public Guid LineManagerId { get; set; }

        public Guid DefaultBackToBackId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? ConfirmDate { get; set; }

        public int DayOn { get; set; }

        public int DayOff { get; set; }

        public int RepeatTimes { get; set; }

        public RelayRotationState State { get; set; }

        public RelayRotationType RotationType { get; set; }

        public virtual ICollection<RelayRotationTopicGroup> RotationTopicGroups { get; set; }

        public virtual ICollection<RelayRotationModules> RotationModules { get; set; }

        public string DefaultBackToBackName { get; set; }

        public string RotationOwnerName { get; set; }
    }
}
