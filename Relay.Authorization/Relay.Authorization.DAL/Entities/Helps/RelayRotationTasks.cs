﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class RelayRotationTasks
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Reference { get; set; }
        public string Description { get; set; }
        public bool IsNullReport { get; set; }
        public bool? IsPinned { get; set; }
        public Guid RelayRotationId { get; set; }
        public DateTime Deadline { get; set; }
        public PriorityOfRelayTask PriorityOfRelayTask { get; set; }
}
}
  