﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class RelayRotationTopic
    {
        public Guid Id { get; set; }

        public DateTime? CreatedUtc { get; set; }

        public DateTime? ModifiedUtc { get; set; }

        public DateTime? DeletedUtc { get; set; }

        public string Name { get; set; }

        public string Variance { get; set; }

        public string Planned { get; set; }

        public string Actual { get; set; }

        public string UnitsSelectedType { get; set; }

        public bool? IsPinned { get; set; }

        public bool IsNullReport { get; set; }

        public string Description { get; set; }

        public string SearchTags { get; set; }

        public bool Enabled { get; set; }

        public Guid? CreatorId { get; set; }

        public Guid RotationTopicGroupId { get; set; }

        public virtual RelayRotationTopicGroup RotationTopicGroup { get; set; }

        public string RelationTopicGroupName { get; set; }
    }
}
