﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class RelayRole : IdentityRole<Guid, RelayUserRole>
    {
        public string Description { get; set; }
    }
}
