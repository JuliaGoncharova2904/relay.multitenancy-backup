﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class RelayUser : IdentityUser<Guid, RelayUserLogin, RelayUserRole, RelayUserClaim>
    {
        public Guid? SubscriptionId { get; set; }

        public virtual Subscription Subscription { get; set; }

        public DateTime? LastLoginDate { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<RelayUser, Guid> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            return userIdentity;
        }
    }
}
