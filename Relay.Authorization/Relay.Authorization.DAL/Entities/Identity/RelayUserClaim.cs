﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class RelayUserClaim : IdentityUserClaim<Guid>
    {
        public virtual RelayUser User { get; set; }
    }
}
