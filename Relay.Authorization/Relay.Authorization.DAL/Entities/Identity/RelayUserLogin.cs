﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class RelayUserLogin : IdentityUserLogin<Guid>
    {
        public Guid Id { get; set; }

        public virtual RelayUser User { get; set; }
    }
}
