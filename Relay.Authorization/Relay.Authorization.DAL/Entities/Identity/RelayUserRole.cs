﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class RelayUserRole : IdentityUserRole<Guid>
    {
        public Guid Id { get; set; }

        public virtual RelayUser User { get; set; }

        public virtual RelayRole Role { get; set; }
    }
}
