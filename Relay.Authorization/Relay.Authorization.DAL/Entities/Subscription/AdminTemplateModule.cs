﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class AdminTemplateModule : HandoverItem
    {
        public TypeOfModule Type { get; set; }

        public bool Enabled { get; set; }

        public Guid? TemplateId { get; set; }

        public virtual AdminTemplates Template { get; set; }

        public Guid? ParentModuleId { get; set; }

        public virtual AdminTemplateModule ParentModule { get; set; }

        public virtual ICollection<AdminTemplateTopicGroups> TopicGroups { get; set; }
    }

    public enum TypeOfModule
    {
        [Description("Safety")]
        HSE = 0,

        [Description("Core")]
        Core = 1
    }

}
