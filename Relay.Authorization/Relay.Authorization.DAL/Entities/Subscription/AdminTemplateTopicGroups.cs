﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class AdminTemplateTopicGroups : HandoverItem
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsNullReport { get; set; }

        public Guid ModuleId { get; set; }

        public Guid? RelationId { get; set; }

        public Guid? ParentTopicGroupId { get; set; }

        public virtual ICollection<AdminTemplateTopics> Topics { get; set; }

        public bool Enabled { get; set; }

        public TypeOfModule Type { get; set; }

        public Guid? TemplateId { get; set; }
    }
}
