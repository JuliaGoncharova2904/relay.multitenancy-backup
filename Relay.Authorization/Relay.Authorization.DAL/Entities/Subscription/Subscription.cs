﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class Subscription
    {
        public Guid Id { get; set; }

        public DateTime CreatedDateUTC { get; set; }

        public DateTime? SubscriptionEndDate { get; set; }

        public Guid OwnerId { get; set; }

        public virtual RelayUser Owner { get; set; }

        public string RelayDbConnection { get; set; }

        public string StripePlanId { get; set; }

        public string TimeZone { get; set; }

        public string StripeManagerId { get; set; }

        public SubscriptionStatus SubscriptionStatus { get; set; }

        public SubscriptionType SubscriptionType { get; set; }

        public RestrictionType RestrictionType { get; set; }

        public BillingPeriod BillingPeriod { get; set; }

        public SubscriptionRestrictions Restrictions { get; set; }

        public virtual ICollection<RelayUser> Users { get; set; }

        public virtual ICollection<SubscriptionPayment> Payments { get; set; }

        public bool ContributorEnabled { get; set; }
    }
}
