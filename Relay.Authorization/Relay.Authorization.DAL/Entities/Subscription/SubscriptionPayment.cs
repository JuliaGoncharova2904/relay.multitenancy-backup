﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class SubscriptionPayment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid SubscriptionId { get; set; }

        public virtual Subscription Subscription { get; set; }

        public DateTime DateTimeUTC { get; set; }

        public decimal Amount { get; set; }

        public string Currency { get; set; }
    }
}
