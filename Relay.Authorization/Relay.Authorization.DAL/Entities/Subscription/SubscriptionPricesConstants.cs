﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class SubscriptionPricesConstants
    {
        public Guid Id { get; set; }

        public decimal FirstManagerPriceForBasicType { get; set; }

        public decimal FirstManagerPriceForPremiumType { get; set; }

        public decimal FirstManagerPriceForUltimateType { get; set; }

        public decimal AdditionalManagerPrice { get; set; }

        public decimal UserPriceForBasicType { get; set; }

        public decimal UserPriceForPremiumType { get; set; }

        public decimal UserPriceForUltimateType { get; set; }

    }
}
