﻿using System.ComponentModel;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public enum SubscriptionStatus
    {
        [Description("Active")]
        Active = 0,

        [Description("Trialing")]
        Trialing = 1,

        [Description("Trial End")]
        TrialEnd = 2,

        [Description("Past Due")]
        PastDue = 3,

        [Description("Unpaid")]
        Unpaid = 4,

        [Description("Canceled")]
        Canceled = 5

    }
}
