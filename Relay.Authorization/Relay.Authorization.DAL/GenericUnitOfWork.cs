﻿using System;
using System.Linq;
using MomentumPlus.Relay.Authorization.Domain.Entities;
using MomentumPlus.Relay.Authorization.Domain.Interfaces;

namespace MomentumPlus.Relay.Authorization.Domain
{
    public class GenericUnitOfWork : IDisposable
    {
        private readonly MultiTenancyContext _dbContext = new MultiTenancyContext();

        private GenericRepository<Subscription> _subscriptionRepository;

        private GenericRepository<SubscriptionPayment> _subscriptionPaymentRepository;

        private SubscriptionPricesConstants _subscriptionPricesConstants;

        private GenericRepository<AdminSectors> _adminSectorsRepository;

        private GenericRepository<AdminTemplates> _adminTemplatesRepository;

        private GenericRepository<AdminTemplateTopicGroups> _adminTemplateTopicGroupsRepository;

        private GenericRepository<AdminTemplateTopics> _adminTemplateTopicsRepository;


        private GenericRepository<RelayRotation> _relayRotationRepository;
        private GenericRepository<RelayRotationTopicGroup> _relayRotationTopicGroupRepository;
        private GenericRepository<RelayRotationTopic> _relayRotationTopicRepository;
        private GenericRepository<RelayRotationModules> _relayRotationModulesRepository;
        private GenericRepository<RelayShift> _relayShiftRepository;
        private GenericRepository<RelayRotationTasks> _relayRotationTasksRepository;
        private GenericRepository<RelayLogoFile> _relayLogoFileRepository;

        public IRepository<RelayLogoFile> RelayLogoFileRepository => _relayLogoFileRepository ?? (_relayLogoFileRepository = new GenericRepository<RelayLogoFile>(_dbContext));
        public IRepository<RelayRotation> RelayRotationRepository => _relayRotationRepository ?? (_relayRotationRepository = new GenericRepository<RelayRotation>(_dbContext));
        public IRepository<RelayRotationTopicGroup> RelayRotationTopicGroupRepository => _relayRotationTopicGroupRepository ?? (_relayRotationTopicGroupRepository = new GenericRepository<RelayRotationTopicGroup>(_dbContext));
        public IRepository<RelayRotationTopic> RelayRotationTopicRepository => _relayRotationTopicRepository ?? (_relayRotationTopicRepository = new GenericRepository<RelayRotationTopic>(_dbContext));
        public IRepository<RelayRotationModules> RelayRotationModulesRepository => _relayRotationModulesRepository ?? (_relayRotationModulesRepository = new GenericRepository<RelayRotationModules>(_dbContext));
        public IRepository<RelayShift> RelayShiftRepository => _relayShiftRepository ?? (_relayShiftRepository = new GenericRepository<RelayShift>(_dbContext));
        public IRepository<RelayRotationTasks> RelayRotationTasksRepository => _relayRotationTasksRepository ?? (_relayRotationTasksRepository = new GenericRepository<RelayRotationTasks>(_dbContext));

        public IRepository<AdminTemplates> AdminTemplatesRepository => _adminTemplatesRepository ?? (_adminTemplatesRepository = new GenericRepository<AdminTemplates>(_dbContext));

        public IRepository<AdminSectors> AdminSectorsRepository => _adminSectorsRepository ?? (_adminSectorsRepository = new GenericRepository<AdminSectors>(_dbContext));

        public IRepository<AdminTemplateTopicGroups> AdminTemplateTopicGroupsRepository => _adminTemplateTopicGroupsRepository ?? (_adminTemplateTopicGroupsRepository = new GenericRepository<AdminTemplateTopicGroups>(_dbContext));

        public IRepository<AdminTemplateTopics> AdminTemplateTopicsRepository => _adminTemplateTopicsRepository ?? (_adminTemplateTopicsRepository = new GenericRepository<AdminTemplateTopics>(_dbContext));

        public IRepository<Subscription> SubscriptionRepository => _subscriptionRepository ?? (_subscriptionRepository = new GenericRepository<Subscription>(_dbContext));

        public IRepository<SubscriptionPayment> SubscriptionPaymentRepository => _subscriptionPaymentRepository
            ?? (_subscriptionPaymentRepository = new GenericRepository<SubscriptionPayment>(_dbContext));


        public SubscriptionPricesConstants SubscriptionPricesConstants
            => _subscriptionPricesConstants ?? _dbContext.SubscriptionPricesConstants.FirstOrDefault();


        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}