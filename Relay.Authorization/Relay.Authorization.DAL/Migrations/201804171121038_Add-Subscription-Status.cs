namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSubscriptionStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.Subscriptions", "SubscriptionStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("multiTenancy.Subscriptions", "SubscriptionStatus");
        }
    }
}
