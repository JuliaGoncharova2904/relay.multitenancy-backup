namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSubscriptionContributorEnabled : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.Subscriptions", "ContributorEnabled", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("multiTenancy.Subscriptions", "ContributorEnabled");
        }
    }
}
