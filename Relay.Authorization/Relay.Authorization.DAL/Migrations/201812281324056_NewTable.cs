namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "multiTenancy.AdminSectors",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        TemplateId = c.Guid(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("multiTenancy.AdminTemplates", t => t.TemplateId)
                .Index(t => t.TemplateId);
            
            CreateTable(
                "multiTenancy.AdminTemplates",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        AdminSectorsId = c.Guid(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("multiTenancy.AdminSectors", "TemplateId", "multiTenancy.AdminTemplates");
            DropIndex("multiTenancy.AdminSectors", new[] { "TemplateId" });
            DropTable("multiTenancy.AdminTemplates");
            DropTable("multiTenancy.AdminSectors");
        }
    }
}
