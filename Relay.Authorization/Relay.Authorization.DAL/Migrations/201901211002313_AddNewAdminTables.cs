namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewAdminTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "multiTenancy.AdminTemplateModules",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        TemplateId = c.Guid(),
                        ParentModuleId = c.Guid(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("multiTenancy.AdminTemplateModules", t => t.ParentModuleId)
                .ForeignKey("multiTenancy.AdminTemplates", t => t.TemplateId)
                .Index(t => t.TemplateId)
                .Index(t => t.ParentModuleId);
            
            CreateTable(
                "multiTenancy.AdminTemplateTopicGroups",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        Description = c.String(),
                        IsNullReport = c.Boolean(nullable: false),
                        ModuleId = c.Guid(nullable: false),
                        RelationId = c.Guid(),
                        ParentTopicGroupId = c.Guid(),
                        Enabled = c.Boolean(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                        AdminTemplateModule_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("multiTenancy.AdminTemplateModules", t => t.AdminTemplateModule_Id)
                .Index(t => t.AdminTemplateModule_Id);
            
            CreateTable(
                "multiTenancy.AdminTemplateTopics",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 150),
                        Description = c.String(),
                        TopicGroupId = c.Guid(nullable: false),
                        ParentTopicId = c.Guid(),
                        Enabled = c.Boolean(nullable: false),
                        UnitsSelectedType = c.String(),
                        IsExpandPlannedActualField = c.Boolean(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("multiTenancy.AdminTemplateTopics", t => t.ParentTopicId)
                .ForeignKey("multiTenancy.AdminTemplateTopicGroups", t => t.TopicGroupId, cascadeDelete: true)
                .Index(t => t.TopicGroupId)
                .Index(t => t.ParentTopicId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("multiTenancy.AdminTemplateTopicGroups", "AdminTemplateModule_Id", "multiTenancy.AdminTemplateModules");
            DropForeignKey("multiTenancy.AdminTemplateTopics", "TopicGroupId", "multiTenancy.AdminTemplateTopicGroups");
            DropForeignKey("multiTenancy.AdminTemplateTopics", "ParentTopicId", "multiTenancy.AdminTemplateTopics");
            DropForeignKey("multiTenancy.AdminTemplateModules", "TemplateId", "multiTenancy.AdminTemplates");
            DropForeignKey("multiTenancy.AdminTemplateModules", "ParentModuleId", "multiTenancy.AdminTemplateModules");
            DropIndex("multiTenancy.AdminTemplateTopics", new[] { "ParentTopicId" });
            DropIndex("multiTenancy.AdminTemplateTopics", new[] { "TopicGroupId" });
            DropIndex("multiTenancy.AdminTemplateTopicGroups", new[] { "AdminTemplateModule_Id" });
            DropIndex("multiTenancy.AdminTemplateModules", new[] { "ParentModuleId" });
            DropIndex("multiTenancy.AdminTemplateModules", new[] { "TemplateId" });
            DropTable("multiTenancy.AdminTemplateTopics");
            DropTable("multiTenancy.AdminTemplateTopicGroups");
            DropTable("multiTenancy.AdminTemplateModules");
        }
    }
}
