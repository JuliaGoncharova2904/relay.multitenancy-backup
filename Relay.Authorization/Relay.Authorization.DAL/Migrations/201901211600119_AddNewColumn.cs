namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.AdminTemplateTopicGroups", "TemplateId", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("multiTenancy.AdminTemplateTopicGroups", "TemplateId");
        }
    }
}
