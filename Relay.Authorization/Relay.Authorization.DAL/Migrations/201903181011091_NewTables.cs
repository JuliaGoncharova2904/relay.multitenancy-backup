namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "multiTenancy.RelayRotations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                        RotationOwnerId = c.Guid(nullable: false),
                        LineManagerId = c.Guid(nullable: false),
                        DefaultBackToBackId = c.Guid(nullable: false),
                        StartDate = c.DateTime(),
                        ConfirmDate = c.DateTime(),
                        DayOn = c.Int(nullable: false),
                        DayOff = c.Int(nullable: false),
                        RepeatTimes = c.Int(nullable: false),
                        State = c.Int(nullable: false),
                        RotationType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "multiTenancy.RelayRotationModules",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                        Type = c.Int(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        SourceType = c.Int(nullable: false),
                        TempateModuleId = c.Guid(),
                        RotationId = c.Guid(),
                        ShiftId = c.Guid(),
                        RelayShift_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("multiTenancy.RelayRotations", t => t.RotationId)
                .ForeignKey("multiTenancy.RelayShifts", t => t.RelayShift_Id)
                .Index(t => t.RotationId)
                .Index(t => t.RelayShift_Id);
            
            CreateTable(
                "multiTenancy.RelayRotationTopicGroups",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                        Name = c.String(),
                        Description = c.String(),
                        RelationId = c.Guid(),
                        RotationId = c.Guid(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        ParentRotationTopicGroupId = c.Guid(),
                        TypeOfModule = c.Int(nullable: false),
                        RotationModuleId = c.Guid(nullable: false),
                        RelayRotation_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("multiTenancy.RelayRotations", t => t.RelayRotation_Id)
                .Index(t => t.RelayRotation_Id);
            
            CreateTable(
                "multiTenancy.RelayRotationTopics",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                        Name = c.String(),
                        Variance = c.String(),
                        Planned = c.String(),
                        Actual = c.String(),
                        UnitsSelectedType = c.String(),
                        IsPinned = c.Boolean(),
                        IsNullReport = c.Boolean(nullable: false),
                        Description = c.String(),
                        SearchTags = c.String(),
                        Enabled = c.Boolean(nullable: false),
                        CreatorId = c.Guid(),
                        RotationTopicGroupId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("multiTenancy.RelayRotationTopicGroups", t => t.RotationTopicGroupId, cascadeDelete: true)
                .Index(t => t.RotationTopicGroupId);
            
            CreateTable(
                "multiTenancy.RelayShifts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                        StartDateTime = c.DateTime(),
                        WorkMinutes = c.Int(nullable: false),
                        BreakMinutes = c.Int(nullable: false),
                        RepeatTimes = c.Int(nullable: false),
                        ShiftRecipientId = c.Guid(),
                        RotationId = c.Guid(nullable: false),
                        State = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("multiTenancy.RelayRotationModules", "RelayShift_Id", "multiTenancy.RelayShifts");
            DropForeignKey("multiTenancy.RelayRotationTopicGroups", "RelayRotation_Id", "multiTenancy.RelayRotations");
            DropForeignKey("multiTenancy.RelayRotationTopics", "RotationTopicGroupId", "multiTenancy.RelayRotationTopicGroups");
            DropForeignKey("multiTenancy.RelayRotationModules", "RotationId", "multiTenancy.RelayRotations");
            DropIndex("multiTenancy.RelayRotationTopics", new[] { "RotationTopicGroupId" });
            DropIndex("multiTenancy.RelayRotationTopicGroups", new[] { "RelayRotation_Id" });
            DropIndex("multiTenancy.RelayRotationModules", new[] { "RelayShift_Id" });
            DropIndex("multiTenancy.RelayRotationModules", new[] { "RotationId" });
            DropTable("multiTenancy.RelayShifts");
            DropTable("multiTenancy.RelayRotationTopics");
            DropTable("multiTenancy.RelayRotationTopicGroups");
            DropTable("multiTenancy.RelayRotationModules");
            DropTable("multiTenancy.RelayRotations");
        }
    }
}
