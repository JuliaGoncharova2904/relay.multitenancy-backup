namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserNameColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.RelayRotations", "DefaultBackToBackName", c => c.String());
            AddColumn("multiTenancy.RelayRotations", "RotationOwnerName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("multiTenancy.RelayRotations", "RotationOwnerName");
            DropColumn("multiTenancy.RelayRotations", "DefaultBackToBackName");
        }
    }
}
