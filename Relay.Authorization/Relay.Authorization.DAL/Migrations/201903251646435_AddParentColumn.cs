namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddParentColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.RelayRotationModules", "ParentModuleId", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("multiTenancy.RelayRotationModules", "ParentModuleId");
        }
    }
}
