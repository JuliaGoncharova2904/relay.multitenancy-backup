namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RelayLogoFileNew : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "multiTenancy.RelayLogoFiles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        BinaryData = c.Binary(),
                        FileType = c.String(),
                        ContentType = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("multiTenancy.RelayLogoFiles");
        }
    }
}
