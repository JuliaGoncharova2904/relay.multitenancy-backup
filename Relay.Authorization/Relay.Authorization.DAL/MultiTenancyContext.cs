﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using MomentumPlus.Relay.Authorization.Domain.Entities;

namespace MomentumPlus.Relay.Authorization.Domain
{
    public class MultiTenancyContext : IdentityDbContext<RelayUser, RelayRole, Guid, RelayUserLogin, RelayUserRole, RelayUserClaim>
    {
        public MultiTenancyContext() : base("MultiTenancyConnection")
        {
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
            Database.SetInitializer(new IdentityInitializer());
        }

        public static MultiTenancyContext Create()
        {
            return new MultiTenancyContext();
        }

        #region Identity

        public DbSet<RelayUserClaim> RelayUserClaims { get; set; }

        public DbSet<RelayUserLogin> RelayUserLogins { get; set; }

        public DbSet<RelayUserRole> RelayUserRoles { get; set; }

        #endregion

        public DbSet<AdminSectors> AdminSectors { get; set; }
        public DbSet<AdminTemplates> AdminTemplates { get; set; }
        public DbSet<AdminTemplateModule> AdminTemplateModule { get; set; }
        public DbSet<AdminTemplateTopicGroups> AdminTemplateTopicGroups { get; set; }
        public DbSet<AdminTemplateTopics> AdminTemplateTopics { get; set; }

        public DbSet<RelayRotation> RelayRotation { get; set; }
        public DbSet<RelayRotationTopicGroup> RelayRotationTopicGroup { get; set; }
        public DbSet<RelayRotationTopic> RelayRotationTopic { get; set; }
        public DbSet<RelayRotationModules> RelayRotationModules { get; set; }
        public DbSet<RelayShift> RelayShift { get; set; }
        public DbSet<RelayRotationTasks> RelayRotationTasks { get; set; }
        public DbSet<RelayLogoFile> RelayLogoFile { get; set; }

        #region Subscription

        public DbSet<Subscription> Subscriptions { get; set; }

        public DbSet<SubscriptionPayment> SubscriptionPaymentLogs { get; set; }

        public DbSet<SubscriptionPricesConstants> SubscriptionPricesConstants { get; set; }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("multiTenancy");

            Identity_EntitiesConfiguration(modelBuilder);

            Subscription_EntitiesConfiguration(modelBuilder);
        }

        private void Identity_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RelayUser>().ToTable("RelayUsers");

            modelBuilder.Entity<RelayUser>()
                        .HasOptional(user => user.Subscription)
                        .WithMany(subscription => subscription.Users)
                        .HasForeignKey(user => user.SubscriptionId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<RelayRole>().ToTable("RelayRoles");
            modelBuilder.Entity<RelayUserRole>().ToTable("RelayUserRoles");
            modelBuilder.Entity<RelayUserLogin>().ToTable("RelayUserLogins");
            modelBuilder.Entity<RelayUserClaim>().ToTable("RelayUserClaims");
        }

        private void Subscription_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Subscription>()
                        .HasRequired(subscription => subscription.Owner)
                        .WithMany()
                        .HasForeignKey(subscription => subscription.OwnerId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubscriptionPayment>()
                        .HasRequired(paymentLog => paymentLog.Subscription)
                        .WithMany(subscription => subscription.Payments)
                        .HasForeignKey(paymentLog => paymentLog.SubscriptionId)
                        .WillCascadeOnDelete(false);
        }

        private void SectorTemplatesModule_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AdminSectors>()
                        .HasOptional(sectorTemplates => sectorTemplates.Template)
                        .WithMany(template => template.AdminSectors)
                        .HasForeignKey(sectorTemplates => sectorTemplates.TemplateId)
                        .WillCascadeOnDelete(true);
        }
    }
}
