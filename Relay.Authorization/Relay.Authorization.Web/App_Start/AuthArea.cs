﻿using System.Web.Mvc;

namespace MomentumPlus.Relay.Authorization
{
    public class AuthArea : AreaRegistration
    {
        public override string AreaName => "Auth";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                 name: "Auth_resources",
                 url: "Auth/resources/{fileName}",
                 defaults: new { controller = "StaticFiles", action = "Index" },
                 namespaces: new[] { "MomentumPlus.Relay.Authorization.Controllers" });

            context.MapRoute(
                 name: "Auth_default",
                 url: "Auth/{controller}/{action}/{id}",
                 defaults: new { controller = "Account", action = "Index", id = UrlParameter.Optional },
                 namespaces: new[] { "MomentumPlus.Relay.Authorization.Controllers" });
        }
    }
}