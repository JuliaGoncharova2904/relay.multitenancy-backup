﻿using System;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using MomentumPlus.Relay.Authorization.Domain.Entities;
using MomentumPlus.Relay.Authorization.Models;
using MomentumPlus.Relay.Helpers;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Authorization.Controllers
{
    public class AccountController : BaseController
    {
        public AccountController()
        {
        }

        /// <summary>
        /// Render Main Page
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return Redirect("~/");
            }
            return View("~/Views/Main.cshtml");
        }

        /// <summary>
        /// Render login view or redirect to home if already logged in.
        /// </summary>
        /// <param name="returnUrl">URL to return to on successful login</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login(string returnUrl, string userName = null)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(new LoginViewModel { ReturnUrl = returnUrl, UserName = userName != null ? userName : null });
        }

        /// <summary>
        /// Handle login form submission
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await UserManager.FindByNameAsync(model.UserName);

            if (user != null)
            {
                if (!await UserManager.IsEmailConfirmedAsync(user.Id))
                {
                    ModelState.AddModelError("Message", @"Please check your e-mail to verify your account.");
                    return View(model);
                }
            }

            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);
          
            Session.Clear();

            switch (result)
            {
                case SignInStatus.Success:
                    Guid userId = Guid.Parse(SignInManager.AuthenticationManager.AuthenticationResponseGrant.Identity.GetUserId());
                    var subscriptionId = UserManager.Users.FirstOrDefault(u => u.Id == userId)?.SubscriptionId;
                    AppSession.Current.CurrentSubscriptionId = subscriptionId;

                    if (!subscriptionId.HasValue || SubscriptionService.IsSubscriptionValid(subscriptionId.Value))
                    {
                        return Redirect(Url.IsLocalUrl(model.ReturnUrl) ? model.ReturnUrl : "/");
                    }
                    ModelState.AddModelError("Message", @"The Subscription is not active. Please speak to your iHandover Consultant.");
                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                    break;

                case SignInStatus.LockedOut:
                    ModelState.AddModelError("Message", @"This account has been deactivated. Please speak to your administrator.");
                    break;
                case SignInStatus.Failure:
                    ModelState.AddModelError("Message", @"The user name or password provided is incorrect.");
                    break;
            }

            return View(model);
        }


        /// <summary>
        /// Log off current user and redirect to login page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Clear();
            Session.Abandon();

            return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Render forgotten password form
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgottenPassword()
        {
            return View(new ForgotPasswordViewModel());
        }

        /// <summary>
        /// Render forgotten password form
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgottenPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = UserManager.FindByName(model.Email);

                if (user != null)
                {
                    var resetUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, resetPasswordHash = user.SecurityStamp }, protocol: Request.Url.Scheme);

                    UserManager.SendEmail(user.Id, "iHandover RelayWorks Password Reset",
                        "Hi. <br /> Someone requested a password reset for your iHandover RelayWorks account. <br /> If you did not request this email please ignore it. <br /> If you did request this email click the URL below to reset your password: <a href=\"" + resetUrl + "\">link</a>");


                    AddAlert(AlertType.Success, @"A password reset email has been sent to your email account, please check your email to continue the process.");

                    return RedirectToAction("Login");
                }

                ModelState.AddModelError("Email", @"No user with that email address exists.");
            }

            return View(model);
        }


        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(string resetPasswordHash)
        {
            RelayUser user = UserManager.Users.SingleOrDefault(u => u.SecurityStamp == resetPasswordHash);

            return (user == null)
                    ? (ActionResult)RedirectToAction("Login")
                    : (ActionResult)View(new ResetPasswordViewModel { PasswordResetGuid = resetPasswordHash });
        }



        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                RelayUser user = UserManager.Users.SingleOrDefault(u => u.SecurityStamp == model.PasswordResetGuid);

                if (user != null && UserManager.RemovePassword(user.Id).Succeeded && UserManager.AddPassword(user.Id, model.NewPassword).Succeeded)
                {
                    AddAlert(AlertType.Success, @"Password changed successfully, you may now log in.");
                }

                return RedirectToAction("Login");
            }

            return View(model);
        }



        [HttpGet]
        [AllowAnonymous]
        public ActionResult UpdatePassword(string updatePasswordHash)
        {
            RelayUser user = UserManager.Users.SingleOrDefault(u => u.SecurityStamp == updatePasswordHash);

            if (user == null)
            {
                return RedirectToAction("Login");
            }
            else
            {
                AddAlert(AlertType.Success, @"E-mail address verified. Please update your password above.");
                return PartialView("~/Views/Account/UpdatePassword.cshtml", new UpdatePasswordViewModel { PasswordResetGuid = updatePasswordHash });
            } 
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult UpdatePassword(UpdatePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                RelayUser user = UserManager.Users.SingleOrDefault(u => u.SecurityStamp == model.PasswordResetGuid);

                if (user != null && UserManager.RemovePassword(user.Id).Succeeded && UserManager.AddPassword(user.Id, model.NewPassword).Succeeded)
                {
                    AddAlert(AlertType.Success, @"Password changed successfully, you may now log in.");
                }

                return RedirectToAction("Login");
            }

            return View(model);
        }


        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> RegisterConfirmation(string userId, string token)
        {
            if (userId == null || token == null)
            {
                AddAlert(AlertType.Danger, @"Please check your e-mail to verify your account.");
                return RedirectToAction("Login");
            }

            var user = await UserManager.FindByIdAsync(Guid.Parse(userId));

            if (user == null)
            {
                AddAlert(AlertType.Danger, @"No user with that email address exists.");
                return RedirectToAction("Login");
            }

            var result = await UserManager.ConfirmEmailAsync(Guid.Parse(userId), token);
            if (result.Succeeded)
            {
                user.EmailConfirmed = true;
            }

            //AddAlert(AlertType.Success, @"Email сonfirm successfully, you may now log in.");
            var userSecurityStamp = UserManager.FindById(Guid.Parse(userId)).SecurityStamp;

            return RedirectToAction("UpdatePassword", new { updatePasswordHash = userSecurityStamp });

        }

    }
}
