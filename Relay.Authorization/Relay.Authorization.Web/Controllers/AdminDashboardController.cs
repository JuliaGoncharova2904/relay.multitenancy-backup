﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Helpers;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Authorization.Controllers
{
    public class AdminDashboardController : BaseController
    {
        /// <summary>
        /// Render Admin Dashboard Page
        /// </summary>
        public ActionResult Index()
        {
            var model = SubscriptionService.GetAllSubscriptions();

            return View("~/Views/AdminDashboard/Dashboard.cshtml", model);
        }


        /// <summary>
        /// Go To Relay
        /// </summary>
        public ActionResult GoToRelay(Guid subscriptionId)
        {
           AppSession.Current.CurrentSubscriptionId = subscriptionId;

            return RedirectToAction("Index", "MyTeam");
        }

        /// <summary>
        /// Extend Trial
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ExtendTrial(Guid subscriptionId)
        {
            SubscriptionService.ExtendTrial(subscriptionId);

            return RedirectToAction("Index");
        }


        /// <summary>
        /// Extend Trial
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StartNewTrial(Guid subscriptionId)
        {
            SubscriptionService.StartTrial(subscriptionId);

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteSubscription(Guid subscriptionId)
        {
            SubscriptionService.DeleteSubscriptionAndUsers(subscriptionId);

            return RedirectToAction("Index");
        }

    }
}