﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Web.Mvc;
using System.Web;
using MomentumPlus.Relay.Authorization.Auth.Managers;
using MomentumPlus.Relay.Authorization.Services;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Authorization.Controllers
{
    [Authorize]
    public abstract class BaseController : Controller
    {
        private readonly RelaySignInManager _signInManager;
        private readonly RelayUserManager _userManager;
        private readonly SubscriptionService _subscriptionService;


        protected BaseController() { }

        protected BaseController(RelayUserManager userManager, RelaySignInManager signInManager)
        {
            this._userManager = userManager;
            this._signInManager = signInManager;
            this._subscriptionService = new SubscriptionService();
        }

        protected RelaySignInManager SignInManager => _signInManager ?? HttpContext.GetOwinContext().Get<RelaySignInManager>();

        protected RelayUserManager UserManager => _userManager ?? HttpContext.GetOwinContext().Get<RelayUserManager>();

        protected IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        protected SubscriptionService SubscriptionService => _subscriptionService ?? new SubscriptionService();

        #region Alerts

        /// <summary>
        /// Add an alert to be shown on the next page loaded.
        /// </summary>
        /// <param name="type">The type of the alert.</param>
        /// <param name="message">Alert message.</param>
        protected void AddAlert(AlertType type, string message)
        {
            var alert = new AlertViewModel
            {
                Type = type,
                Message = message
            };

            List<AlertViewModel> alerts = TempData.ContainsKey("Alerts")
                ? (List<AlertViewModel>)TempData["Alerts"]
                : new List<AlertViewModel>();

            alerts.Insert(0, alert);

            TempData["Alerts"] = alerts;
        }

        #endregion


    }
}