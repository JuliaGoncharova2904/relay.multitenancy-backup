﻿using System.Web.Mvc;
using MomentumPlus.Relay.Authorization.Services;
using MomentumPlus.Relay.Authorization.Services.Registration;
using MomentumPlus.Relay.Authorization.Helpers;
using MomentumPlus.Relay.Authorization.Models;
using System.Threading.Tasks;
using System;
using System.Web.Configuration;

namespace MomentumPlus.Relay.Authorization.Controllers
{
    public class RegistrationController : BaseController
    {
        /// <summary>
        /// Render Sign Up Page
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Summary");
            }

            return View("~/Views/Registration/Registration.cshtml");
        }



        [HttpPost]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public async Task <ActionResult> SignUp(RegistrationViewModel model)
        {
            if (ModelState.IsValid)
            {
                RegistrationMediator mediator = new RegistrationMediator();

                RegistrationStep validateExpDateStep = new ValidateExpDateRegistrationStep(mediator);
                RegistrationStep paymentStep = new PaymentRegistrationStep(mediator);
                RegistrationStep registrationUserStep = new IdentityUserRegistrationStep(mediator);
                RegistrationStep subscriptionCreationStep = new SubscriptionCreationRegistrationStep(mediator);
                RegistrationStep initializeRelayStep = new InitializeRelayRegistrationStep(mediator);

                mediator.ValidateExpDateStep = validateExpDateStep;
                mediator.PaymentStep = paymentStep;
                mediator.RegistrationUserStep = registrationUserStep;
                mediator.SubscriptionCreationStep = subscriptionCreationStep;
                mediator.InitializeRelayRegistrationStep = initializeRelayStep;

                validateExpDateStep.Execute(model, ModelState);
                paymentStep.Execute(model, ModelState);
                registrationUserStep.Execute(model, ModelState);
                subscriptionCreationStep.Execute(model, ModelState);
                initializeRelayStep.Execute(model, ModelState);

                var errorsMessage = ModelState.ErrorsMessage();

                if (!Boolean.Parse(WebConfigurationManager.AppSettings["isInTestMode"]) && errorsMessage == null)
                {
                    var token = await UserManager.GenerateEmailConfirmationTokenAsync(mediator.CreatedUser.Id);
                    var callbackUrl = Url.Action("RegisterConfirmation", "Account", new { userId = mediator.CreatedUser.Id, token = token }, protocol: Request.Url.Scheme);

                    await SignInManager.UserManager.SendEmailAsync(mediator.CreatedUser.Id, "Thank you for purchasing RelayWorks",
                    " Hello, " + model.Name + "! <br />" +
                    " Thank you for purchasing RelayWorks. <br />" +
                    " To activate you account, please follow this <a href=\"" + callbackUrl + "\">link</a>" +
                    " <br /> Kind Regards, <br /> " +
                    " RelayWorks Team");
                }
                
                if (ModelState.IsValid)
                {
                    return RedirectToAction("InitializeRelay", "Home");
                }
            }

            return Json(new { status = false, data = new { message = ModelState.ErrorsMessage(), errors = ModelState.Errors() } }, JsonRequestBehavior.AllowGet);
        }



        /// <summary>
        /// Price Constants
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult PriceConstants()
        {
            var model = SubscriptionService.GetPricesConstants();

            return Json(model);
        }



        /// <summary>
        /// Sign Up Step 1
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Step1()
        {
            return PartialView("~/Views/Registration/_Step_1.cshtml");
        }

        /// <summary>
        /// Sign Up Step 2
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Step2()
        {
            return PartialView("~/Views/Registration/_Step_2.cshtml");
        }


        /// <summary>
        /// Sign Up Step 3
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Step3()
        {
            return PartialView("~/Views/Registration/_Step_3.cshtml");
        }


        /// <summary>
        /// Sign Up Step 1
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Step4()
        {
            return PartialView("~/Views/Registration/_Step_4.cshtml");
        }
    }
}