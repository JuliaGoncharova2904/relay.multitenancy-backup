﻿using MomentumPlus.Relay.Authorization.Helpers;
using MomentumPlus.Relay.Authorization.Models;
using MomentumPlus.Relay.Authorization.Services;
using MomentumPlus.Relay.Authorization.Services.Registration;
using System.Threading.Tasks;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;
using System;
using System.Web.Configuration;

namespace MomentumPlus.Relay.Authorization.Controllers
{
    public class TrialController :  BaseController
    {

        public ActionResult TrialRegistration()
        {
            return View("~/Views/Trial/RegistrationTrial.cshtml");
        }

        [HttpPost]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public async Task<ActionResult> SignUp(RegistrationTrialViewModel model)
        {
            if (ModelState.IsValid)
            {
                RegistrationMediator mediator = new RegistrationMediator();

                RegistrationStep registrationUserStep = new IdentityUserRegistrationStep(mediator);
                RegistrationStep subscriptionCreationStep = new SubscriptionCreationRegistrationStep(mediator);
                RegistrationStep initializeRelayStep = new InitializeRelayRegistrationStep(mediator);

                mediator.RegistrationUserStep = registrationUserStep;
                mediator.SubscriptionCreationStep = subscriptionCreationStep;
                mediator.InitializeRelayRegistrationStep = initializeRelayStep;

                registrationUserStep.Execute(model, ModelState);
                subscriptionCreationStep.Execute(model, ModelState);
                initializeRelayStep.Execute(model, ModelState);

                if (!Boolean.Parse(WebConfigurationManager.AppSettings["isInTestMode"]))
                {
                    var token = await UserManager.GenerateEmailConfirmationTokenAsync(mediator.CreatedUser.Id);
                    var callbackUrl = Url.Action("RegisterConfirmation", "Account", new { userId = mediator.CreatedUser.Id, token = token }, protocol: Request.Url.Scheme);

                    await SignInManager.UserManager.SendEmailAsync(mediator.CreatedUser.Id, "Thank you for purchasing RelayWorks",
                    " Hello, " + model.Name + "! <br />" +
                    " Thank you for purchasing RelayWorks. <br />" +
                    " To activate you account, please follow this <a href=\"" + callbackUrl + "\">link</a>" +
                    " <br /> Kind Regards, <br /> " +
                    " RelayWorks Team");
                }

                if (ModelState.IsValid)
                {
                    return RedirectToAction("InitializeRelay", "Home");
                }
            }

            return Json(new { status = false, data = new { message = ModelState.ErrorsMessage(), errors = ModelState.Errors() } }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Price Constants
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult PriceConstants()
        {
            var model = SubscriptionService.GetPricesConstants();

            return Json(model);
        }



        /// <summary>
        /// Trial Step 1
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Step1()
        {
            return PartialView("~/Views/Trial/_Step_1.cshtml");
        }

        /// <summary>
        /// Trial Step 2
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Step2()
        {
            return PartialView("~/Views/Trial/_Step_2.cshtml");
        }


        /// <summary>
        /// Trial Step 3
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Step3()
        {
            return PartialView("~/Views/Trial/_Step_3.cshtml");
        }



        /// <summary>
        /// Go to login
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login()
        {
            return RedirectToAction("Index", "AdminDashboard");
        }


        [HttpPost]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult PayForTrial(UpgradeSubscriptionViewModel model)
        {
            if (ModelState.IsValid)
            {
                RegistrationMediator mediator = new RegistrationMediator();

                RegistrationStep validateExpDateStep = new ValidateExpDateRegistrationStep(mediator);
                RegistrationStep paymentStep = new PaymentRegistrationStep(mediator);
                RegistrationStep subscriptionStep = new SubscriptionCreationRegistrationStep(mediator);

                mediator.ValidateExpDateStep = validateExpDateStep;
                mediator.PaymentStep = paymentStep;
                mediator.SubscriptionCreationStep = subscriptionStep;

                validateExpDateStep.Execute(model, ModelState);
                paymentStep.Execute(model, ModelState);
                subscriptionStep.Execute(model, ModelState);

                if (ModelState.IsValid)
                {

                    return Json(new { status = true, data = new { message = "Upgrade Success" } }, JsonRequestBehavior.AllowGet);
                }

            }

            return Json(new { status = false, data = new { message = ModelState.ErrorsMessage(), errors = ModelState.Errors() } }, JsonRequestBehavior.AllowGet);
        }
    }
}