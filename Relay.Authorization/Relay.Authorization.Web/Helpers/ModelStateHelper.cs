﻿using System.Collections;
using System.Linq;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Authorization.Helpers
{
    public static class ModelStateHelper
    {
        public static IEnumerable Errors(this ModelStateDictionary modelState)
        {
            if (!modelState.IsValid)
            {
                return modelState.ToDictionary(kvp => kvp.Key,
                    kvp => kvp.Value.Errors
                                    .Select(e => e.ErrorMessage).ToArray())
                                    .Where(m => m.Value.Any());
            }
            return null;
        }

        public static string ErrorsMessage(this ModelStateDictionary modelState)
        {
            if (!modelState.IsValid)
            {

                var messages = modelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);

                return messages.Aggregate("", (str, obj) => str+ "<p>" + obj.ToString() + "</p>");
            }
            return null;
        }
    }
}