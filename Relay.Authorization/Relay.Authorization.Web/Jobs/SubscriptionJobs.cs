﻿using System;
using System.Linq;
using MomentumPlus.Relay.Authorization.Domain;
using MomentumPlus.Relay.Authorization.Domain.Entities;

namespace MomentumPlus.Relay.Authorization
{
    public class SubscriptionJobs
    {
        private readonly GenericUnitOfWork _unitOfWork;

        public SubscriptionJobs()
        {
            this._unitOfWork = new GenericUnitOfWork();
        }
        public void EndTrialSubscriptions()
        {
            var subscriptionsWithEndingTrial = _unitOfWork.SubscriptionRepository.GetAll().Where(s => s.SubscriptionStatus == SubscriptionStatus.Trialing && s.SubscriptionEndDate < DateTime.Now);

            foreach (var subscription in subscriptionsWithEndingTrial)
            {
                subscription.SubscriptionStatus = SubscriptionStatus.TrialEnd;
                _unitOfWork.SubscriptionRepository.Update(subscription);
            }

            //var subscriptionsWithPastDue = _unitOfWork.SubscriptionRepository.GetAll().Where(s => s.SubscriptionStatus == SubscriptionStatus.Active && s.SubscriptionEndDate < DateTime.Now);

            //foreach (var subscription in subscriptionsWithPastDue)
            //{
            //    subscription.SubscriptionStatus = SubscriptionStatus.PastDue;
            //    _unitOfWork.SubscriptionRepository.Update(subscription);
            //}

            _unitOfWork.SaveChanges();
        }
    }
}
