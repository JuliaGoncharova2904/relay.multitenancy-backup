﻿using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Authorization.Models
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email*")]
        public string Email { get; set; }
    }
}
