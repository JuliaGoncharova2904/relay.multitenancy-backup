﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Authorization.Models
{
    public class PriceConstantsViewModel
    {
        public decimal FirstManagerPriceForBasicType { get; set; }

        public decimal FirstManagerPriceForPremiumType { get; set; }

        public decimal FirstManagerPriceForUltimateType { get; set; }

        public decimal AdditionalManagerPrice { get; set; }

        public decimal UserPriceForBasicType { get; set; }

        public decimal UserPriceForPremiumType { get; set; }

        public decimal UserPriceForUltimateType { get; set; }

        public string[] Countries { get; set; }

        public string[] TimeZones { get; set; }
}
}
