﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MomentumPlus.Relay.Authorization.Domain.Entities;

namespace MomentumPlus.Relay.Authorization.Models
{
    public class RegistrationViewModel
    {
        [Required]
        [Display(Name = "E-mail*")]
        [EmailAddress]
        [StringLength(256, MinimumLength = 5, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Surname*")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Surname { get; set; }


        [Required]
        [Display(Name = "Company*")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Company { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string OfficeLocation { get; set; }

        [Required]
        [Display(Name = "Job Position*")]
        [StringLength(50, MinimumLength = 3)]
        public string Position { get; set; }

        [Required]
        [Display(Name = "Password*")]
        [RegularExpression(
            @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$",
            ErrorMessage = @"Your password must be 8 characters and contain a mixture of upper and lower-case letters, numbers and special characters such as !, #, @ or ?"
        )]
        public string Password { get; set; }

        [Required]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The passwords do not match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password*")]
        public string PasswordConfirm { get; set; }


        [Required]
        [Display(Name = "Card Number*")]
        [StringLength(50, MinimumLength = 3)]
        public string CardNumber { get; set; }


        [Required]
        [Display(Name = "Name on Card*")]
        [StringLength(50, MinimumLength = 3)]
        public string NameOnCard { get; set; }


        [Display(Name = "Start Date")]
        public string StartDate { get; set; }


        [Required]
        [Display(Name = "Exp Date*")]
        public string ExpDate { get; set; }



        [Display(Name = "Issue Number")]
        public string IssueNumber { get; set; }

        [Required]
        [Display(Name = "CVC*")]
        [StringLength(3, MinimumLength = 3)]
        public string CVC { get; set; }


        [Required]
        public int SimpleUsersCount { get; set; }

        [Required]
        public int ManagersUsersCount { get; set; }


        [Required]
        public SubscriptionType UserSubscriptionType { get; set; }


        [Required]
        public RestrictionType SubscriptionLevelType { get; set; }


        [Required]
        public BillingPeriod BillingPeriod { get; set; }

        [Required]
        public bool IsAgree { get; set; }

        [Display(Name = "VAT*")]
        [StringLength(16, MinimumLength = 2)]
        public string VAT { get; set; }

        //IEnumerable<SelectListItem> Countries { get; set; }

        [Required(ErrorMessage = "Please select country")]
        [Display(Name = "Country*")]
        public string UserCountry { get; set; }

        [Required(ErrorMessage = "Please select TimeZone")]
        [Display(Name = "TimeZone*")]
        public string TimeZone { get; set; }

        public string StringError { get; set; }
    }

}
