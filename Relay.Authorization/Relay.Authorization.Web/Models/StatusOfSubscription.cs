﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MomentumPlus.Relay.Authorization.Models
{
    public enum StatusOfSubscription
    {
        [Description("Active")]
        Active = 0,

        [Description("Trialing")]
        Trialing = 1,

        [Description("Trial End")]
        TrialEnd = 2,

        [Description("Past Due")]
        PastDue = 3,

        [Description("Unpaid")]
        Unpaid = 4,

        [Description("Canceled")]
        Canceled = 5
    }
}