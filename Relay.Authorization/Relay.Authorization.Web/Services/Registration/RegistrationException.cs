﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MomentumPlus.Relay.Authorization.Services
{
    [Serializable]
    public class RegistrationException : Exception
    {
        public RegistrationException()
        : base() { }

        public RegistrationException(string message)
        : base(message) { }

        public RegistrationException(string format, params object[] args)
        : base(string.Format(format, args)) { }

        public RegistrationException(string message, Exception innerException)
        : base(message, innerException) { }

        public RegistrationException(string format, Exception innerException, params object[] args)
        : base(string.Format(format, args), innerException) { }

        protected RegistrationException(SerializationInfo info, StreamingContext context)
        : base(info, context) { }
    }
}