﻿using System;
using System.Web.Mvc;
using MomentumPlus.Relay.Authorization.Models;
using MomentumPlus.Relay.Models;
using NLog;
using RegistrationViewModel = MomentumPlus.Relay.Authorization.Models.RegistrationViewModel;

namespace MomentumPlus.Relay.Authorization.Services.Registration
{
    class RegistrationMediator : Mediator
    {
        public RegistrationStep ValidateExpDateStep { get; set; }

        public RegistrationStep PaymentStep { get; set; }

        public RegistrationStep RegistrationUserStep { get; set; }

        public RegistrationStep SubscriptionCreationStep { get; set; }

        public RegistrationStep InitializeRelayRegistrationStep { get; set; }


        private static readonly Logger ErrorLogger = LogManager.GetLogger("MultiTenancy.Registration.Error");

        public override void Execute(RegistrationViewModel registrationData, ModelStateDictionary modelState, RegistrationStep registrationStep)
        {
            try
            {
                if (modelState.IsValid)
                {
                    if (registrationStep == ValidateExpDateStep)
                    {
                        ValidateExpDateStep.Run(registrationData, modelState);
                    }

                    if (registrationStep == PaymentStep)
                    {
                        PaymentStep.Run(registrationData, modelState);
                    }

                    if (registrationStep == RegistrationUserStep)
                    {
                        RegistrationUserStep.Run(registrationData, modelState);
                    }

                    if (registrationStep == SubscriptionCreationStep)
                    {
                        SubscriptionCreationStep.Run(registrationData, modelState);
                        RegistrationUserStep.Update();

                    }
                    if (registrationStep == InitializeRelayRegistrationStep)
                    {
                        InitializeRelayRegistrationStep.Run(registrationData, modelState);
                    }
                }
            }
            catch (Exception exception)
            {
                PaymentStep.Rollback();
                RegistrationUserStep.Rollback();
                SubscriptionCreationStep.Rollback();

                ErrorLogger.Error(exception, "Registration Error.");
            }
        }

        public override void Execute(RegistrationTrialViewModel registrationData, ModelStateDictionary modelState, RegistrationStep registrationStep)
        {
            try
            {
                if (modelState.IsValid)
                {
                    if (registrationStep == PaymentStep)
                    {
                        PaymentStep.Run(registrationData, modelState);
                    }

                    if (registrationStep == RegistrationUserStep)
                    {
                        RegistrationUserStep.Run(registrationData, modelState);
                    }

                    if (registrationStep == SubscriptionCreationStep)
                    {
                        SubscriptionCreationStep.Run(registrationData, modelState);
                        RegistrationUserStep.Update();

                    }
                    if (registrationStep == InitializeRelayRegistrationStep)
                    {
                        InitializeRelayRegistrationStep.Run(registrationData, modelState);
                    }
                }
            }
            catch (Exception exception)
            {
                PaymentStep.Rollback();
                RegistrationUserStep.Rollback();
                SubscriptionCreationStep.Rollback();

                ErrorLogger.Error(exception, "Registration Error.");
            }
        }

        public override void Execute(UpgradeSubscriptionViewModel upgradeData, ModelStateDictionary modelState, RegistrationStep registrationStep)
        {
            try
            {
                if (modelState.IsValid)
                {
                    if (registrationStep == ValidateExpDateStep)
                    {
                        ValidateExpDateStep.Run(upgradeData, modelState);
                    }

                    if (registrationStep == PaymentStep)
                    {
                        PaymentStep.Run(upgradeData, modelState);
                    }


                    if (registrationStep == SubscriptionCreationStep)
                    {
                        SubscriptionCreationStep.Run(upgradeData, modelState);
                    }
                }
            }
            catch (Exception exception)
            {
                PaymentStep.Rollback();
                SubscriptionCreationStep.Rollback();

                ErrorLogger.Error(exception, "Registration Error.");
            }
        }
    }
}