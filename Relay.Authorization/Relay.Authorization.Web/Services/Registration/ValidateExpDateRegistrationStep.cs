﻿using System;
using System.Web.Mvc;
using MomentumPlus.Relay.Authorization.Models;
using MomentumPlus.Relay.Models;
using RegistrationViewModel = MomentumPlus.Relay.Authorization.Models.RegistrationViewModel;

namespace MomentumPlus.Relay.Authorization.Services.Registration
{
    class ValidateExpDateRegistrationStep : RegistrationStep
    {
        public ValidateExpDateRegistrationStep(Mediator mediator) : base(mediator) { }

        public override void Run(RegistrationViewModel registrationData, ModelStateDictionary modelState)
        {
            try
            {
                DateTime dateTime = DateTime.ParseExact(registrationData.ExpDate, "MM/yy", System.Globalization.CultureInfo.InvariantCulture);

                if (dateTime <= DateTime.Now)
                {
                    modelState.AddModelError("ExpDate", @"Expiration Date not valid");
                    //throw new RegistrationException(@"Expiration Date not valid");
                }            
            }
            catch
            {
                modelState.AddModelError("ExpDate", @"Expiration Date not valid");
                throw new RegistrationException(@"Error when parse Date");
            }
        }

        public override void Run(UpgradeSubscriptionViewModel upgradeData, ModelStateDictionary modelState)
        {
            try
            {
                DateTime dateTime = DateTime.ParseExact(upgradeData.ExpDate, "MM/yy", System.Globalization.CultureInfo.InvariantCulture);

                if (dateTime <= DateTime.Now)
                {
                    modelState.AddModelError("ExpDate", @"Expiration Date not valid");
                }
            }
            catch
            {
                modelState.AddModelError("ExpDate", @"Expiration Date not valid");
                throw new RegistrationException(@"Error when parse Date");
            }
        }

        public override void Run(RegistrationTrialViewModel registrationData, ModelStateDictionary modelState)
        {
            try
            {
               
            }
            catch
            {
               
            }
        }

        public override void Rollback()
        {
        }

        public override void Update()
        {
        }
    }
}