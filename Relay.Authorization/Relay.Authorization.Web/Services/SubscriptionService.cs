﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Relay.Authorization.Adapter;
using MomentumPlus.Relay.Authorization.Constants;
using MomentumPlus.Relay.Authorization.Domain;
using MomentumPlus.Relay.Authorization.Domain.Entities;
using MomentumPlus.Relay.Authorization.Models;
using MomentumPlus.Relay.Extensions;
using MomentumPlus.Relay.Models;
using NLog;
using Stripe;
using RegistrationViewModel = MomentumPlus.Relay.Authorization.Models.RegistrationViewModel;

namespace MomentumPlus.Relay.Authorization.Services
{
    public class SubscriptionService
    {
        private readonly GenericUnitOfWork _unitOfWork;

        public SubscriptionService()
        {
            this._unitOfWork = new GenericUnitOfWork();
        }

        public PriceConstantsViewModel GetPricesConstants()
        {
            SubscriptionPricesConstants constants = _unitOfWork.SubscriptionPricesConstants;

            PriceConstantsViewModel model = new PriceConstantsViewModel
            {
                FirstManagerPriceForBasicType = 0,
                FirstManagerPriceForPremiumType = 0,
                FirstManagerPriceForUltimateType = 0,
                AdditionalManagerPrice = 0,
                UserPriceForBasicType = 12,
                UserPriceForPremiumType = 24/*constants.UserPriceForPremiumType*/,
                UserPriceForUltimateType = 30/*constants.UserPriceForUltimateType*/
            };

            model.Countries = GetAllCountries();

            model.TimeZones = TimeZoneInfo.GetSystemTimeZones().Select(zone => zone.DisplayName).ToArray();

            return model;
        }


        private string[] GetAllCountries()
        {
            Dictionary<string, string> objDic = new Dictionary<string, string>();

            foreach (CultureInfo ObjCultureInfo in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                RegionInfo objRegionInfo = new RegionInfo(ObjCultureInfo.Name);
                if (!objDic.ContainsKey(objRegionInfo.EnglishName))
                {
                    objDic.Add(objRegionInfo.EnglishName, objRegionInfo.TwoLetterISORegionName.ToLower());
                }
            }

            var obj = objDic.OrderBy(p => p.Key);
            var y = obj.Select(t => t.Key);
            return y.ToArray();
        }



        public void DeleteSubscription(Guid subscriptionId)
        {
            Subscription subscription = _unitOfWork.SubscriptionRepository.Get(subscriptionId);

            if (subscription != null)
            {
                _unitOfWork.SubscriptionRepository.Delete(subscription);
            }
        }

        /// <summary>
        /// Check Subscription not completed and paid
        /// </summary>
        public bool IsSubscriptionValid(Guid subscriptionId)
        {
            Subscription subscription = _unitOfWork.SubscriptionRepository.Get(subscriptionId);

            if (subscription != null)
            {
                if (subscription.SubscriptionStatus == SubscriptionStatus.Trialing)
                {
                    return true;
                }

                if (subscription.SubscriptionStatus == SubscriptionStatus.Active)
                {

                    if (subscription.StripeManagerId != null)
                    {
                        var stripeCustomerService = new StripeCustomerService(StripeConstants.StripeApiKey);

                        var stripeUser = stripeCustomerService.Get(subscription.StripeManagerId);

                        var stripeSubscription = stripeUser?.Subscriptions.FirstOrDefault();

                        if (stripeSubscription != null)
                        {
                            UpdateSubscription(subscription, stripeSubscription);

                            if (stripeSubscription.Status == "active" || stripeSubscription.Status == "trialing")
                            {
                                return true;
                            }
                        }
                    }

                    else
                    {
                        if (subscription.SubscriptionEndDate >= DateTime.Now)
                        {
                            return true;
                        }
                    }
                }


                return false;

            }

            return false;
        }


        public void UpdateSubscription(Subscription subscription, StripeSubscription stripeSubscription)
        {
            subscription.SubscriptionStatus = (SubscriptionStatus)Enum.Parse(typeof(SubscriptionStatus), stripeSubscription.Status, ignoreCase: true);

            if (stripeSubscription.CurrentPeriodEnd.HasValue)

            {
                subscription.SubscriptionEndDate = stripeSubscription.CurrentPeriodEnd;
            }

            _unitOfWork.SubscriptionRepository.Update(subscription);

            _unitOfWork.SaveChanges();

        }

        public void UpdateStripePlanSubscription(Subscription subscription)
        {
            _unitOfWork.SubscriptionRepository.Update(subscription);

            _unitOfWork.SaveChanges();

        }

        public IEnumerable<SubscriptionViewModel> GetAllSubscriptions()
        {
            var subscriptions = _unitOfWork.SubscriptionRepository.GetAll();

            List<SubscriptionViewModel> result = new List<SubscriptionViewModel>();

            foreach (var subscription in subscriptions)
            {
                result.Add(new SubscriptionViewModel
                {
                    SubscriptionId = subscription.Id,
                    ManagerEmail = subscription.Owner.UserName,
                    BillingPeriod = subscription.BillingPeriod.GetEnumDescription(),
                    RestrictionType = subscription.RestrictionType.GetEnumDescription(),
                    SubscriptionType = subscription.SubscriptionType.GetEnumDescription(),
                    CreatedDate = subscription.CreatedDateUTC.ToString("dd MMM yyyy"),
                    EndDate = subscription.SubscriptionEndDate?.ToString("dd MMM yyyy") ?? "-",
                    Users = subscription.Users.Count + "/" + (subscription.Restrictions.MaxUsers + subscription.Restrictions.MaxManagers),
                    SubscriptionStatus = (StatusOfSubscription)subscription.SubscriptionStatus,
                    EndDateTime = subscription.SubscriptionEndDate
                });

                if (subscription.SubscriptionStatus == SubscriptionStatus.Canceled)
                {
                    var authService = new AuthService();
                    authService.CancelSubscription(subscription.OwnerId);
                }
            }

            return result;
        }

        public Guid CreateSubscription(Guid subscriptionOwnerId,
           RegistrationViewModel model,
            string stripeUserId,
            string stripePlanId,
            int ammount,
            bool save)
        {
            Subscription subscription = new Subscription
            {
                Id = Guid.NewGuid(),
                OwnerId = subscriptionOwnerId,
                CreatedDateUTC = DateTime.UtcNow,
                SubscriptionType = model.UserSubscriptionType,
                RestrictionType = model.SubscriptionLevelType,
                StripeManagerId = stripeUserId,
                StripePlanId = stripePlanId,
                BillingPeriod = model.BillingPeriod,
                SubscriptionEndDate = model.BillingPeriod == BillingPeriod.Month ? DateTime.UtcNow.AddMonths(1) : DateTime.UtcNow.AddYears(1),
                Restrictions = new SubscriptionRestrictions(),
                ContributorEnabled = model.SubscriptionLevelType != RestrictionType.Basic ? true : false,
                TimeZone = model.TimeZone
            };

            subscription.RelayDbConnection = "Relay_" + subscription.Id;

            InitializationSubscriptionRestrictions(subscription, model.UserSubscriptionType, model.SubscriptionLevelType, model.SimpleUsersCount, model.ManagersUsersCount);

            _unitOfWork.SubscriptionRepository.Add(subscription);

            SubscriptionPayment payment = new SubscriptionPayment
            {
                Id = Guid.NewGuid(),
                SubscriptionId = subscription.Id,
                Currency = "usd",
                DateTimeUTC = DateTime.UtcNow,
                Amount = ammount / 100
            };

            _unitOfWork.SubscriptionPaymentRepository.Add(payment);

            if (save)
            {
                _unitOfWork.SaveChanges();

            }

            return subscription.Id;
        }



        public Guid CreateSubscription(Guid subscriptionOwnerId,
          RegistrationTrialViewModel model,
           string stripeUserId,
           string stripePlanId,
           int ammount,
           bool save)
        {
            Subscription subscription = new Subscription
            {
                Id = Guid.NewGuid(),
                OwnerId = subscriptionOwnerId,
                CreatedDateUTC = DateTime.UtcNow,
                SubscriptionType = model.UserSubscriptionType,
                RestrictionType = model.SubscriptionLevelType,
                StripeManagerId = stripeUserId,
                StripePlanId = stripePlanId,
                BillingPeriod = model.BillingPeriod,
                SubscriptionEndDate = model.BillingPeriod == BillingPeriod.Month ? DateTime.UtcNow.AddMonths(1) : DateTime.UtcNow.AddYears(1),
                Restrictions = new SubscriptionRestrictions(),
                ContributorEnabled = model.SubscriptionLevelType != RestrictionType.Basic ? true : false
            };

            subscription.RelayDbConnection = "Relay_" + subscription.Id;

            InitializationSubscriptionRestrictions(subscription, model.UserSubscriptionType, model.SubscriptionLevelType, model.SimpleUsersCount, model.ManagersUsersCount);

            _unitOfWork.SubscriptionRepository.Add(subscription);


            SubscriptionPayment payment = new SubscriptionPayment
            {
                Id = Guid.NewGuid(),
                SubscriptionId = subscription.Id,
                Currency = "gbp",
                DateTimeUTC = DateTime.UtcNow,
                Amount = ammount / 100
            };

            _unitOfWork.SubscriptionPaymentRepository.Add(payment);

            if (save)
            {
                _unitOfWork.SaveChanges();

            }
            return subscription.Id;
        }


        public Guid CreateTrialSubscription(Guid subscriptionOwnerId,
         RegistrationTrialViewModel model,
          string stripeUserId,
          string stripePlanId,
          int ammount,
          bool save)
        {
            Subscription subscription = new Subscription
            {
                Id = Guid.NewGuid(),
                OwnerId = subscriptionOwnerId,
                CreatedDateUTC = DateTime.UtcNow,
                SubscriptionType = model.UserSubscriptionType,
                RestrictionType = model.SubscriptionLevelType,
                StripeManagerId = stripeUserId,
                StripePlanId = stripePlanId,
                BillingPeriod = model.BillingPeriod,
                SubscriptionEndDate = DateTime.UtcNow.AddDays(14),
                SubscriptionStatus = SubscriptionStatus.Trialing,
                Restrictions = new SubscriptionRestrictions()
            };

            subscription.RelayDbConnection = "Relay_" + subscription.Id;

            InitializationSubscriptionRestrictions(subscription, model.UserSubscriptionType, model.SubscriptionLevelType, model.SimpleUsersCount, model.ManagersUsersCount);

            _unitOfWork.SubscriptionRepository.Add(subscription);

            if (save)
            {
                _unitOfWork.SaveChanges();

            }

            return subscription.Id;
        }


        public int? NumberOfRemainingTrialDays(Guid subscriptionId)
        {
            Subscription subscription = _unitOfWork.SubscriptionRepository.Get(subscriptionId);

            if (subscription != null)
            {
                if (subscription.SubscriptionStatus == SubscriptionStatus.Trialing && subscription.SubscriptionEndDate.HasValue)
                {
                    return ((TimeSpan)(subscription.SubscriptionEndDate.Value - subscription.CreatedDateUTC)).Days;
                }
                else
                {
                    return (int?)null;
                }
            }
            else
            {
                return (int?)null;
            }
        }



        private void InitializationSubscriptionRestrictions(Subscription subscription,
            SubscriptionType subscriptionType,
            RestrictionType restrictionType,
            int userCount,
            int managersCount)
        {
            switch (subscriptionType)
            {
                case SubscriptionType.Single:
                    subscription.Restrictions.MaxUsers = 1;
                    subscription.Restrictions.MaxManagers = 0;
                    PopulateSubscriptionRestrictions(subscription, restrictionType);
                    break;
                case SubscriptionType.BackToBack:
                    subscription.Restrictions.MaxUsers = 2;
                    subscription.Restrictions.MaxManagers = 0;
                    PopulateSubscriptionRestrictions(subscription, restrictionType);
                    break;
                case SubscriptionType.Team:
                    subscription.Restrictions.MaxUsers = userCount;
                    subscription.Restrictions.MaxManagers = 0;
                    PopulateSubscriptionRestrictions(subscription, restrictionType);
                    break;
            }
        }



        private void PopulateSubscriptionRestrictions(Subscription subscription, RestrictionType restrictionType)
        {
            switch (restrictionType)
            {
                case RestrictionType.Basic:
                    subscription.Restrictions.MaxCarryForwards = 0;
                    subscription.Restrictions.MaxEmailShares = 0;
                    subscription.Restrictions.MaxMonthArchives = 0;
                    subscription.Restrictions.MaxHandoverItems = 10;
                    subscription.Restrictions.MaxHandoverTasks = 3;
                    subscription.ContributorEnabled = false;
                    subscription.Restrictions.CompanyBranding = false;
                    break;
                case RestrictionType.Premium:
                    subscription.Restrictions.MaxCarryForwards = 3;
                    subscription.Restrictions.MaxEmailShares = 3;
                    subscription.Restrictions.MaxMonthArchives = 6;
                    subscription.Restrictions.MaxHandoverItems = 20;
                    subscription.Restrictions.MaxHandoverTasks = 10;
                    subscription.ContributorEnabled = true;
                    subscription.Restrictions.CompanyBranding = true;
                    break;
                case RestrictionType.Ultimate:
                    subscription.Restrictions.MaxCarryForwards = null;
                    subscription.Restrictions.MaxEmailShares = null;
                    subscription.Restrictions.MaxMonthArchives = null;
                    subscription.Restrictions.MaxHandoverItems = null;
                    subscription.Restrictions.MaxHandoverTasks = null;
                    subscription.ContributorEnabled = true;
                    subscription.Restrictions.CompanyBranding = true;
                    break;
            }
        }

        ////////////////////////////////////////////////////////////
        /// 
        public void ExtendTrial(Guid subscriptionId)
        {
            Subscription subscription = _unitOfWork.SubscriptionRepository.Get(subscriptionId);

            if (subscription?.SubscriptionStatus == SubscriptionStatus.Trialing)
            {
                subscription.SubscriptionEndDate = DateTime.UtcNow.AddDays(14);

                subscription.SubscriptionStatus = SubscriptionStatus.TrialEnd;

                _unitOfWork.SubscriptionRepository.Update(subscription);

                _unitOfWork.SaveChanges();
            }
            else
            {
                throw new Exception("Subscription not found, or Status is not Trialing.");
            }
        }

        public void StartTrial(Guid subscriptionId)
        {
            Subscription subscription = _unitOfWork.SubscriptionRepository.Get(subscriptionId);

            if (subscription?.SubscriptionStatus == SubscriptionStatus.TrialEnd)
            {
                subscription.SubscriptionEndDate = DateTime.UtcNow.AddDays(14);
                subscription.SubscriptionStatus = SubscriptionStatus.Trialing;

                _unitOfWork.SubscriptionRepository.Update(subscription);

                _unitOfWork.SaveChanges();
            }
            else
            {
                throw new Exception("Subscription not found, or Status is not Trial End.");
            }
        }


        public void UpdateTrialSubscription(UpgradeSubscriptionViewModel model, string stripeUserId, string stripePlanId, int ammount)
        {
            if (model.SubscriptionId.HasValue)
            {
                Subscription subscription = _unitOfWork.SubscriptionRepository.Get(model.SubscriptionId.Value);

                if (subscription != null)
                {
                    subscription.StripeManagerId = stripeUserId;
                    subscription.StripePlanId = stripePlanId;
                    subscription.SubscriptionEndDate = DateTime.UtcNow.AddMonths(1);
                    subscription.ContributorEnabled = model.SubscriptionLevelType != SubscriptionRestrictionType.Basic;
                    subscription.RestrictionType = (RestrictionType)model.SubscriptionLevelType;
                    subscription.SubscriptionStatus = SubscriptionStatus.Active;
                    subscription.Restrictions.MaxUsers = model.NumberOfusers;
                    subscription.RestrictionType = (RestrictionType)model.SubscriptionLevelType;


                    PopulateSubscriptionRestrictions(subscription, (RestrictionType)model.SubscriptionLevelType);

                    _unitOfWork.SubscriptionRepository.Update(subscription);

                    SubscriptionPayment payment = new SubscriptionPayment
                    {
                        Id = Guid.NewGuid(),
                        SubscriptionId = subscription.Id,
                        Currency = "gbp",
                        DateTimeUTC = DateTime.UtcNow,
                        Amount = ammount / 100
                    };

                    _unitOfWork.SubscriptionPaymentRepository.Add(payment);

                    _unitOfWork.SaveChanges();

                }
                else
                {
                    throw new Exception($"Subscription {model.SubscriptionId.Value} not found!");
                }
            }

            else
            {
                throw new Exception("Subscription Id null!");
            }

        }

        public void DeleteSubscriptionAndUsers(Guid subscriptionId)
        {
            Subscription subscription = _unitOfWork.SubscriptionRepository.Get(subscriptionId);

            if (subscription != null)
            {
                var authService = new AuthService();

                var connectionString = string.Format(ConfigurationManager.ConnectionStrings["RelayConnection"].ConnectionString, subscription?.RelayDbConnection);

                List<Guid> subscriptionUsersIds = subscription.Users.Select(u => u.Id).ToList();


                try
                {
                    if (!string.IsNullOrEmpty(subscription.StripeManagerId))
                    {
                        var stripeCustomerService = new StripeCustomerService(StripeConstants.StripeApiKey);
                        stripeCustomerService.Delete(subscription.StripeManagerId);

                        if (!string.IsNullOrEmpty(subscription.StripePlanId))
                        {
                            var stripePlanService = new StripePlanService(StripeConstants.StripeApiKey);
                            stripePlanService.Delete(subscription.StripePlanId);
                        }

                    }
                }
                catch (Exception)
                {

                }

                if (subscription.Payments.ToList().Any())
                {

                    foreach (var payment in subscription.Payments.ToList())
                    {
                        _unitOfWork.SubscriptionPaymentRepository.Delete(payment);

                    }
                }
                _unitOfWork.SubscriptionRepository.Delete(subscription);


                _unitOfWork.SaveChanges();


                foreach (var userId in subscriptionUsersIds)
                {
                    authService.RemoveUser(userId, new Guid());

                }


                try
                {
                    Database.Delete(connectionString);

                }
                catch (Exception)
                {

                }
                finally
                {
                    DeleteLogger.Warn($"Subscription id: - {subscriptionId} was deleting.");
                }


            }

        }

        public void SaveSectors(AdminSectors adminSectors)
        {
            var sectors = _unitOfWork.AdminSectorsRepository.GetAll().ToList();

            if (sectors.Find(t => t.Id == adminSectors.Id) == null)
            {
                _unitOfWork.AdminSectorsRepository.Add(adminSectors);
                _unitOfWork.SaveChanges();
            }
        }

        public void SaveTemplates(AdminTemplates adminTemplates)
        {
            var templates = _unitOfWork.AdminTemplatesRepository.GetAll().AsNoTracking().ToList();

            if (templates.Find(t => t.Id == adminTemplates.Id) == null)
            {
                _unitOfWork.AdminTemplatesRepository.Add(adminTemplates);
                _unitOfWork.SaveChanges();
            }
        }

        public List<AdminSectors> GetAdminSectors()
        {
            return _unitOfWork.AdminSectorsRepository.GetAll().AsNoTracking().ToList();
        }

        public List<AdminTemplates> GetAdminTemplates(Guid? sectorId)
        {
            if(sectorId.HasValue)
            {
                return _unitOfWork.AdminTemplatesRepository.GetAll().AsNoTracking().Where(t => t.AdminSectorsId == sectorId).ToList();
            }

            return _unitOfWork.AdminTemplatesRepository.GetAll().AsNoTracking().ToList();
        }

        public AdminTemplates GetAdminTemplateById(Guid templateId)
        {
            return _unitOfWork.AdminTemplatesRepository.Get(templateId);
        }

        public List<AdminTemplateTopicGroups> GetAdminTopicGroupsByType(TypeOfModule type)
        {
            return _unitOfWork.AdminTemplateTopicGroupsRepository.GetAll().Where(t => t.Type == type).ToList();
        }

        public List<AdminTemplateTopics> GetAdminTopicsByType(TypeOfModule type)
        {
            return _unitOfWork.AdminTemplateTopicsRepository.GetAll().Where(t => t.Type == type).ToList();
        }

        public void SaveAdminTopicGroups(AdminTemplateTopicGroups adminTemplateTopicGroups)
        {
            var topicGroups = _unitOfWork.AdminTemplateTopicGroupsRepository.GetAll().AsNoTracking().ToList();

            if (topicGroups.Find(t => t.Id == adminTemplateTopicGroups.Id) == null)
            {
                _unitOfWork.AdminTemplateTopicGroupsRepository.Add(adminTemplateTopicGroups);
                _unitOfWork.SaveChanges();
            }
        }

        public void SaveAdminTopics(AdminTemplateTopics adminTemplateTopics)
        {
            var topicGroups = _unitOfWork.AdminTemplateTopicsRepository.GetAll().AsNoTracking().ToList();

            if (topicGroups.Find(t => t.Id == adminTemplateTopics.Id) == null)
            {
                _unitOfWork.AdminTemplateTopicsRepository.Add(adminTemplateTopics);
                _unitOfWork.SaveChanges();
            }
        }

        public List<AdminTemplateTopicGroups> GetAdminTopicGroups(Authorization.Domain.Entities.TypeOfModule moduleType, Guid templateId)
        {
            return _unitOfWork.AdminTemplateTopicGroupsRepository.GetAll().Where(t => t.TemplateId == templateId && t.Type == moduleType).ToList();
        }


        public Guid? GetSectorByTemplateId(Guid templateId)
        {
            return _unitOfWork.AdminTemplatesRepository.GetAll().AsNoTracking().FirstOrDefault(t => t.Id == templateId).AdminSectorsId;
        }

        public RelayRotationModules GetRotationModule(TypeOfModule type, Guid rotationId, TypeOfModuleSource sourceType)
        {
            return _unitOfWork.RelayRotationModulesRepository.GetAll().FirstOrDefault(m => m.RotationId == rotationId && m.Type == type && !m.DeletedUtc.HasValue &&
                                                                                                                                                m.SourceType == sourceType);
        }

        public RelayRotationModules GetShiftRotationModule(TypeOfModule type, Guid shiftId, TypeOfModuleSource sourceType)
        {                                                                                                                            
            return _unitOfWork.RelayRotationModulesRepository.GetAll().FirstOrDefault(m => m.ShiftId != null && m.ShiftId == shiftId && m.Type == type && !m.DeletedUtc.HasValue &&
                                                                                                                                                        m.SourceType == sourceType);
        }

        public List<RelayRotationTopicGroup> GetRotationTopicGroups(Guid rotationId, Guid moduleId)
        {
            return _unitOfWork.RelayRotationTopicGroupRepository.GetAll().Where(rt => rt.RotationId == rotationId && rt.RotationModuleId == moduleId).ToList();
        }

        public List<RelayRotationTopicGroup> GetShiftRotationTopicGroups(Guid shiftId, Guid moduleId)
        {
            return _unitOfWork.RelayRotationTopicGroupRepository.GetAll().Where(rt => rt.ShiftId == shiftId && rt.RotationModuleId == moduleId).ToList();
        }

        public RelayRotationTopicGroup GetRelayRotationTopicGroup(Guid topicGroupId)
        {
            return _unitOfWork.RelayRotationTopicGroupRepository.GetAll().FirstOrDefault(rt => rt.Id == topicGroupId);
        }


        public List<RelayRotationTopic> GetRotationTopics(Guid topicGroupId)
        {
            return _unitOfWork.RelayRotationTopicRepository.GetAll().Where(rt => rt.RotationTopicGroupId == topicGroupId).ToList();
        }

        public void CopyToRelayShift(Core.Models.Shift shift)
        {
            RelayShift relayShift = new RelayShift
            {
                BreakMinutes = shift.BreakMinutes,
                CreatedUtc = shift.CreatedUtc,
                DefaultBackToBackName = shift.ShiftRecipient.FullName,
                DeletedUtc = shift.DeletedUtc,
                Id = shift.Id,
                ModifiedUtc = shift.ModifiedUtc,
                RepeatTimes = shift.RepeatTimes,
                RotationOwnerName = shift.Rotation.RotationOwner.FullName,
                ShiftRecipientId = shift.ShiftRecipientId,
                StartDateTime = shift.StartDateTime,
                State = (ShiftState)shift.State,
                WorkMinutes = shift.WorkMinutes,
                RotationId = shift.RotationId
            };

            if (_unitOfWork.RelayShiftRepository.GetAll().FirstOrDefault(r => r.Id == relayShift.Id) == null)
            {
                _unitOfWork.RelayShiftRepository.Add(relayShift);
                _unitOfWork.SaveChanges();
            }
            else
            {
                var existShift = _unitOfWork.RelayShiftRepository.GetAll().FirstOrDefault(r => r.Id == relayShift.Id);

                RelayShift newRelayShift = new RelayShift
                {
                    BreakMinutes = shift.BreakMinutes,
                    CreatedUtc = shift.CreatedUtc,
                    DefaultBackToBackName = shift.ShiftRecipient.FullName,
                    DeletedUtc = shift.DeletedUtc,
                    Id = shift.Id,
                    ModifiedUtc = shift.ModifiedUtc,
                    RepeatTimes = shift.RepeatTimes,
                    RotationOwnerName = shift.Rotation.RotationOwner.FullName,
                    ShiftRecipientId = shift.ShiftRecipientId,
                    StartDateTime = shift.StartDateTime,
                    State = (ShiftState)shift.State,
                    WorkMinutes = shift.WorkMinutes,
                    RotationId = shift.RotationId
                };

                if (existShift != newRelayShift)
                {
                    _unitOfWork.RelayShiftRepository.Delete(existShift.Id);

                    foreach (var rotationModule in shift.RotationModules)
                    {
                        RelayRotationModules relayRotationModules = new RelayRotationModules
                        {
                            CreatedUtc = rotationModule.CreatedUtc,
                            DeletedUtc = rotationModule.DeletedUtc,
                            Enabled = rotationModule.Enabled,
                            Id = Guid.NewGuid(),
                            ModifiedUtc = rotationModule.ModifiedUtc,
                            ShiftId = shift.Id,
                            SourceType = (TypeOfModuleSource)rotationModule.SourceType,
                            Type = (TypeOfModule)rotationModule.Type,
                            IsShift = true,
                            ParentModuleId = rotationModule.Id
                        };

                        _unitOfWork.RelayRotationModulesRepository.Delete(relayRotationModules.Id);
                    }

                    _unitOfWork.SaveChanges();

                    if (_unitOfWork.RelayShiftRepository.GetAll().FirstOrDefault(r => r.Id == newRelayShift.Id) == null)
                    {
                        _unitOfWork.RelayShiftRepository.Add(newRelayShift);
                        _unitOfWork.SaveChanges();
                    }
                }
            }

            foreach (var rotationModule in shift.RotationModules)
            {
                RelayRotationModules relayRotationModules = new RelayRotationModules
                {
                    CreatedUtc = rotationModule.CreatedUtc,
                    DeletedUtc = rotationModule.DeletedUtc,
                    Enabled = rotationModule.Enabled,
                    Id = Guid.NewGuid(),
                    ModifiedUtc = rotationModule.ModifiedUtc,
                    ShiftId = shift.Id,
                    SourceType = (TypeOfModuleSource)rotationModule.SourceType,
                    Type = (TypeOfModule)rotationModule.Type,
                    IsShift = true,
                    ParentModuleId = rotationModule.Id
                };

                if (_unitOfWork.RelayRotationModulesRepository.GetAll().FirstOrDefault(r => r.Id == relayRotationModules.Id) == null)
                {
                    _unitOfWork.RelayRotationModulesRepository.Add(relayRotationModules);
                    _unitOfWork.SaveChanges();
                }
            }

            var topicGroups = shift.RotationModules.SelectMany(r => r.RotationTopicGroups).ToList();

            if (topicGroups.Any())
            {
                foreach (var topicGroup in topicGroups)
                {
                    RelayRotationTopicGroup relayRotationTopicGroup = new RelayRotationTopicGroup
                    {
                        CreatedUtc = topicGroup.CreatedUtc,
                        DeletedUtc = topicGroup.DeletedUtc,
                        Description = topicGroup.Description,
                        Enabled = topicGroup.Enabled,
                        Id = topicGroup.Id,
                        ModifiedUtc = topicGroup.ModifiedUtc,
                        Name = topicGroup.Name,
                        ParentRotationTopicGroupId = topicGroup.ParentRotationTopicGroupId,
                        ShiftId = shift.Id,
                        RotationModuleId = topicGroup.RotationModuleId
                    };

                    if (_unitOfWork.RelayRotationTopicGroupRepository.GetAll().FirstOrDefault(r => r.Id == relayRotationTopicGroup.Id) == null)
                    {
                        _unitOfWork.RelayRotationTopicGroupRepository.Add(relayRotationTopicGroup);
                        _unitOfWork.SaveChanges();
                    }
                    else
                    {
                        var existTopicGroup = _unitOfWork.RelayRotationTopicGroupRepository.GetAll().FirstOrDefault(r => r.Id == relayRotationTopicGroup.Id);

                        RelayRotationTopicGroup newRelayRotationTopicGroup = new RelayRotationTopicGroup
                        {
                            CreatedUtc = topicGroup.CreatedUtc,
                            DeletedUtc = topicGroup.DeletedUtc,
                            Description = topicGroup.Description,
                            Enabled = topicGroup.Enabled,
                            Id = topicGroup.Id,
                            ModifiedUtc = topicGroup.ModifiedUtc,
                            Name = topicGroup.Name,
                            ParentRotationTopicGroupId = topicGroup.ParentRotationTopicGroupId,
                            ShiftId = shift.Id,
                            RotationModuleId = topicGroup.RotationModuleId
                        };

                        if (existTopicGroup != newRelayRotationTopicGroup)
                        {
                            _unitOfWork.RelayRotationTopicGroupRepository.Delete(relayRotationTopicGroup.Id);
                            _unitOfWork.SaveChanges();

                            _unitOfWork.RelayRotationTopicGroupRepository.Add(newRelayRotationTopicGroup);
                            _unitOfWork.SaveChanges();
                        }
                    }
                }
            }

            var topics = shift.RotationModules.SelectMany(r => r.RotationTopicGroups).SelectMany(t => t.RotationTopics).ToList();

            if (topics.Any())
            {
                foreach (var topic in topics)
                {
                    RelayRotationTopic relayRotationTopic = new RelayRotationTopic
                    {
                        Actual = topic.Actual,
                        CreatedUtc = topic.CreatedUtc,
                        CreatorId = topic.CreatorId,
                        DeletedUtc = topic.DeletedUtc,
                        Description = topic.Description,
                        Enabled = topic.Enabled,
                        Id = topic.Id,
                        IsNullReport = topic.IsNullReport,
                        IsPinned = topic.IsPinned,
                        ModifiedUtc = topic.ModifiedUtc,
                        Name = topic.Name,
                        Planned = topic.Planned,
                        RotationTopicGroupId = topic.RotationTopicGroupId,
                        SearchTags = topic.SearchTags,
                        UnitsSelectedType = topic.UnitsSelectedType,
                        Variance = topic.Variance
                    };

                    if (_unitOfWork.RelayRotationTopicRepository.GetAll().FirstOrDefault(r => r.Id == relayRotationTopic.Id) == null)
                    {
                        _unitOfWork.RelayRotationTopicRepository.Add(relayRotationTopic);
                        _unitOfWork.SaveChanges();
                    }
                    else
                    {
                        var existTopic = _unitOfWork.RelayRotationTopicRepository.GetAll().FirstOrDefault(r => r.Id == relayRotationTopic.Id);

                        RelayRotationTopic newRelayRotationTopic = new RelayRotationTopic
                        {
                            Actual = topic.Actual,
                            CreatedUtc = topic.CreatedUtc,
                            CreatorId = topic.CreatorId,
                            DeletedUtc = topic.DeletedUtc,
                            Description = topic.Description,
                            Enabled = topic.Enabled,
                            Id = topic.Id,
                            IsNullReport = topic.IsNullReport,
                            IsPinned = topic.IsPinned,
                            ModifiedUtc = topic.ModifiedUtc,
                            Name = topic.Name,
                            Planned = topic.Planned,
                            RotationTopicGroupId = topic.RotationTopicGroupId,
                            SearchTags = topic.SearchTags,
                            UnitsSelectedType = topic.UnitsSelectedType,
                            Variance = topic.Variance
                        };

                        if (existTopic != newRelayRotationTopic)
                        {
                            _unitOfWork.RelayRotationTopicRepository.Delete(existTopic.Id);
                            _unitOfWork.SaveChanges();

                            _unitOfWork.RelayRotationTopicRepository.Add(newRelayRotationTopic);
                            _unitOfWork.SaveChanges();
                        }
                    }
                }
            }

        }

        public void CopyToRelaySettings(Core.Models.AdminSettings adminSettings, Guid rotationId)
        {
            if (adminSettings.Logo != null)
            {
                RelayLogoFile relayLogoFile = new RelayLogoFile
                {
                    BinaryData = adminSettings.Logo.BinaryData,
                    ContentType = adminSettings.Logo.ContentType,
                    FileType = adminSettings.Logo.FileType,
                    Id = adminSettings.Logo.Id,
                    RotationId = rotationId
                };

                if (_unitOfWork.RelayLogoFileRepository.GetAll().FirstOrDefault(r => r.Id == relayLogoFile.Id) == null)
                {
                    _unitOfWork.RelayLogoFileRepository.Add(relayLogoFile);
                    _unitOfWork.SaveChanges();
                }
                else
                {
                    var existFileLogo = _unitOfWork.RelayLogoFileRepository.GetAll().FirstOrDefault(r => r.Id == relayLogoFile.Id);

                    RelayLogoFile newRelayLogoFile = new RelayLogoFile
                    {
                        BinaryData = adminSettings.Logo.BinaryData,
                        ContentType = adminSettings.Logo.ContentType,
                        FileType = adminSettings.Logo.FileType,
                        Id = adminSettings.Logo.Id,
                        RotationId = rotationId
                    };

                    if (existFileLogo != newRelayLogoFile)
                    {
                        _unitOfWork.RelayLogoFileRepository.Delete(existFileLogo.Id);
                        _unitOfWork.SaveChanges();

                        _unitOfWork.RelayLogoFileRepository.Add(newRelayLogoFile);
                        _unitOfWork.SaveChanges();
                    }
                }
            }
        }


        public void CopyToRelayRotation(Core.Models.Rotation rotation)
        {
            RelayRotation relayRotation = new RelayRotation
            {
                ConfirmDate = rotation.ConfirmDate,
                CreatedUtc = rotation.CreatedUtc,
                DayOff = rotation.DayOff,
                DayOn = rotation.DayOn,
                DefaultBackToBackId = rotation.DefaultBackToBackId,
                DeletedUtc = rotation.DeletedUtc,
                Id = rotation.Id,
                LineManagerId = rotation.LineManagerId,
                ModifiedUtc = rotation.ModifiedUtc,
                RepeatTimes = rotation.RepeatTimes,
                RotationOwnerId = rotation.RotationOwnerId,
                RotationType = (RelayRotationType)rotation.RotationType,
                StartDate = rotation.StartDate,
                State = (RelayRotationState)rotation.State,
                RotationOwnerName = rotation.RotationOwner.FullName,
                DefaultBackToBackName = rotation.DefaultBackToBack.FullName
            };

            if (_unitOfWork.RelayRotationRepository.GetAll().FirstOrDefault(r => r.Id == relayRotation.Id) == null)
            {
                _unitOfWork.RelayRotationRepository.Add(relayRotation);
                _unitOfWork.SaveChanges();
            }
            else
            {
                var existRotation = _unitOfWork.RelayRotationRepository.GetAll().FirstOrDefault(r => r.Id == relayRotation.Id);

                RelayRotation newRelayRotation = new RelayRotation
                {
                    ConfirmDate = rotation.ConfirmDate,
                    CreatedUtc = rotation.CreatedUtc,
                    DayOff = rotation.DayOff,
                    DayOn = rotation.DayOn,
                    DefaultBackToBackId = rotation.DefaultBackToBackId,
                    DeletedUtc = rotation.DeletedUtc,
                    Id = rotation.Id,
                    LineManagerId = rotation.LineManagerId,
                    ModifiedUtc = rotation.ModifiedUtc,
                    RepeatTimes = rotation.RepeatTimes,
                    RotationOwnerId = rotation.RotationOwnerId,
                    RotationType = (RelayRotationType)rotation.RotationType,
                    StartDate = rotation.StartDate,
                    State = (RelayRotationState)rotation.State,
                    RotationOwnerName = rotation.RotationOwner.FullName,
                    DefaultBackToBackName = rotation.DefaultBackToBack.FullName
                };

                if (existRotation != newRelayRotation)
                {
                    _unitOfWork.RelayRotationRepository.Delete(existRotation.Id);

                    foreach (var rotationModule in rotation.RotationModules)
                    {
                        RelayRotationModules relayRotationModules = new RelayRotationModules
                        {
                            CreatedUtc = rotationModule.CreatedUtc,
                            DeletedUtc = rotationModule.DeletedUtc,
                            Enabled = rotationModule.Enabled,
                            Id = rotationModule.Id,
                            ModifiedUtc = rotationModule.ModifiedUtc,
                            RotationId = rotation.Id,
                            SourceType = (TypeOfModuleSource)rotationModule.SourceType,
                            Type = (TypeOfModule)rotationModule.Type
                        };

                        _unitOfWork.RelayRotationModulesRepository.Delete(relayRotationModules.Id);
                    }

                    _unitOfWork.SaveChanges();

                    if (_unitOfWork.RelayRotationRepository.GetAll().FirstOrDefault(r => r.Id == newRelayRotation.Id) == null)
                    {
                        _unitOfWork.RelayRotationRepository.Add(newRelayRotation);
                        _unitOfWork.SaveChanges();
                    }
                }
            }

            foreach (var rotationModule in rotation.RotationModules)
            {
                RelayRotationModules relayRotationModules = new RelayRotationModules
                {
                    CreatedUtc = rotationModule.CreatedUtc,
                    DeletedUtc = rotationModule.DeletedUtc,
                    Enabled = rotationModule.Enabled,
                    Id = rotationModule.Id,
                    ModifiedUtc = rotationModule.ModifiedUtc,
                    RotationId = rotation.Id,
                    SourceType = (TypeOfModuleSource)rotationModule.SourceType,
                    Type = (TypeOfModule)rotationModule.Type
                };

                if (_unitOfWork.RelayRotationModulesRepository.GetAll().FirstOrDefault(r => r.Id == relayRotationModules.Id) == null)
                {
                    _unitOfWork.RelayRotationModulesRepository.Add(relayRotationModules);
                    _unitOfWork.SaveChanges();
                }
            }

            var topicGroups = rotation.RotationModules.SelectMany(r => r.RotationTopicGroups).ToList();

            if (topicGroups.Any())
            {
                foreach (var topicGroup in topicGroups)
                {
                    RelayRotationTopicGroup relayRotationTopicGroup = new RelayRotationTopicGroup
                    {
                        CreatedUtc = topicGroup.CreatedUtc,
                        DeletedUtc = topicGroup.DeletedUtc,
                        Description = topicGroup.Description,
                        Enabled = topicGroup.Enabled,
                        Id = topicGroup.Id,
                        ModifiedUtc = topicGroup.ModifiedUtc,
                        Name = topicGroup.Name,
                        ParentRotationTopicGroupId = topicGroup.ParentRotationTopicGroupId,
                        RotationId = relayRotation.Id,
                        RotationModuleId = topicGroup.RotationModuleId
                    };

                    if (_unitOfWork.RelayRotationTopicGroupRepository.GetAll().FirstOrDefault(r => r.Id == relayRotationTopicGroup.Id) == null)
                    {
                        _unitOfWork.RelayRotationTopicGroupRepository.Add(relayRotationTopicGroup);
                        _unitOfWork.SaveChanges();
                    }
                    else
                    {
                        var existTopicGroup = _unitOfWork.RelayRotationTopicGroupRepository.GetAll().FirstOrDefault(r => r.Id == relayRotationTopicGroup.Id);

                        RelayRotationTopicGroup newRelayRotationTopicGroup = new RelayRotationTopicGroup
                        {
                            CreatedUtc = topicGroup.CreatedUtc,
                            DeletedUtc = topicGroup.DeletedUtc,
                            Description = topicGroup.Description,
                            Enabled = topicGroup.Enabled,
                            Id = topicGroup.Id,
                            ModifiedUtc = topicGroup.ModifiedUtc,
                            Name = topicGroup.Name,
                            ParentRotationTopicGroupId = topicGroup.ParentRotationTopicGroupId,
                            RotationId = relayRotation.Id,
                            RotationModuleId = topicGroup.RotationModuleId
                        };

                        if (existTopicGroup != newRelayRotationTopicGroup)
                        {
                            _unitOfWork.RelayRotationTopicGroupRepository.Delete(relayRotationTopicGroup.Id);
                            _unitOfWork.SaveChanges();

                            _unitOfWork.RelayRotationTopicGroupRepository.Add(newRelayRotationTopicGroup);
                            _unitOfWork.SaveChanges();
                        }
                    }
                }
            }

            var topics = rotation.RotationModules.SelectMany(r => r.RotationTopicGroups).SelectMany(t => t.RotationTopics).ToList();

            if (topics.Any())
            {
                foreach (var topic in topics)
                {
                    RelayRotationTopic relayRotationTopic = new RelayRotationTopic
                    {
                        Actual = topic.Actual,
                        CreatedUtc = topic.CreatedUtc,
                        CreatorId = topic.CreatorId,
                        DeletedUtc = topic.DeletedUtc,
                        Description = topic.Description,
                        Enabled = topic.Enabled,
                        Id = topic.Id,
                        IsNullReport = topic.IsNullReport,
                        IsPinned = topic.IsPinned,
                        ModifiedUtc = topic.ModifiedUtc,
                        Name = topic.Name,
                        Planned = topic.Planned,
                        RotationTopicGroupId = topic.RotationTopicGroupId,
                        SearchTags = topic.SearchTags,
                        UnitsSelectedType = topic.UnitsSelectedType,
                        Variance = topic.Variance
                    };

                    if (_unitOfWork.RelayRotationTopicRepository.GetAll().FirstOrDefault(r => r.Id == relayRotationTopic.Id) == null)
                    {
                        _unitOfWork.RelayRotationTopicRepository.Add(relayRotationTopic);
                        _unitOfWork.SaveChanges();
                    }
                    else
                    {
                        var existTopic = _unitOfWork.RelayRotationTopicRepository.GetAll().FirstOrDefault(r => r.Id == relayRotationTopic.Id);

                        RelayRotationTopic newRelayRotationTopic = new RelayRotationTopic
                        {
                            Actual = topic.Actual,
                            CreatedUtc = topic.CreatedUtc,
                            CreatorId = topic.CreatorId,
                            DeletedUtc = topic.DeletedUtc,
                            Description = topic.Description,
                            Enabled = topic.Enabled,
                            Id = topic.Id,
                            IsNullReport = topic.IsNullReport,
                            IsPinned = topic.IsPinned,
                            ModifiedUtc = topic.ModifiedUtc,
                            Name = topic.Name,
                            Planned = topic.Planned,
                            RotationTopicGroupId = topic.RotationTopicGroupId,
                            SearchTags = topic.SearchTags,
                            UnitsSelectedType = topic.UnitsSelectedType,
                            Variance = topic.Variance
                        };

                        if (existTopic != newRelayRotationTopic)
                        {
                            _unitOfWork.RelayRotationTopicRepository.Delete(existTopic.Id);
                            _unitOfWork.SaveChanges();

                            _unitOfWork.RelayRotationTopicRepository.Add(newRelayRotationTopic);
                            _unitOfWork.SaveChanges();
                        }
                    }
                }
            }
        }

        public void CopyToRelayRotationTask(Core.Models.RotationTask rotationTask, Guid rotationId)
        {
            RelayRotationTasks relayRotationTasks = new RelayRotationTasks
            {
                Deadline = rotationTask.Deadline,
                Description = rotationTask.Description,
                Id = rotationTask.Id,
                IsNullReport = rotationTask.IsNullReport,
                IsPinned = rotationTask.IsPinned,
                Name = rotationTask.Name,
                PriorityOfRelayTask = (PriorityOfRelayTask)rotationTask.Priority,
                Reference = rotationTask.RotationTopic != null ? rotationTask.RotationTopic.Name : null,
                RelayRotationId = rotationId
            };

            if (_unitOfWork.RelayRotationTasksRepository.GetAll().FirstOrDefault(r => r.Id == relayRotationTasks.Id) == null)
            {
                _unitOfWork.RelayRotationTasksRepository.Add(relayRotationTasks);
                _unitOfWork.SaveChanges();
            }
        }

        public List<RelayRotationTasks> GetRelayRotationTask(Guid rotationId)
        {
            return _unitOfWork.RelayRotationTasksRepository.GetAll().Where(t => t.RelayRotationId == rotationId).ToList();
        }

        public RelayRotation GetRelayRotation(Guid rotationId)
        {
            var rotation = _unitOfWork.RelayRotationRepository.Get(rotationId);

            if (rotation != null)
            {
                var modules = _unitOfWork.RelayRotationModulesRepository.GetAll().Where(r => r.RotationId == rotation.Id).ToList();

                var topicGroups = _unitOfWork.RelayRotationTopicGroupRepository.GetAll().Where(rt => rt.RotationId == rotation.Id).ToList();

                foreach (var topicGroup in rotation.RotationTopicGroups)
                {
                    var topics = topicGroups.SelectMany(t => t.RotationTopics).ToList();

                    topics = _unitOfWork.RelayRotationTopicRepository.GetAll().Where(t => t.RotationTopicGroupId == topicGroup.Id).ToList();
                }

                rotation.RotationModules = modules;
                rotation.RotationTopicGroups = topicGroups;
            }

            return rotation;
        }

        public RelayShift GetRelayShift(Guid shiftId)
        {
            return _unitOfWork.RelayShiftRepository.GetAll().FirstOrDefault(sh => sh.Id == shiftId);
        }

        public RelayLogoFile GetRelayLogoFile(Guid rotationId)
        {
            return _unitOfWork.RelayLogoFileRepository.GetAll().FirstOrDefault(f=>f.RotationId == rotationId);
        }

        public void UpdatePaymentSubscription(Guid subscriptionId, StripePlan stripePlan)
        {
            Subscription subscription = _unitOfWork.SubscriptionRepository.Get(subscriptionId); 

            if (subscription != null)
            {
                subscription.StripePlanId = stripePlan.Id;

                _unitOfWork.SubscriptionRepository.Update(subscription);

                var payment = _unitOfWork.SubscriptionPaymentRepository.GetAll().FirstOrDefault(p => p.SubscriptionId == subscriptionId);
                payment.Currency = "gbp";
                payment.Amount = (decimal)stripePlan.Amount;

                _unitOfWork.SubscriptionPaymentRepository.Update(payment);

                _unitOfWork.SaveChanges();
            }
        }

        private static readonly Logger DeleteLogger = LogManager.GetLogger("Subscription.DeleteLogger");

    }
}