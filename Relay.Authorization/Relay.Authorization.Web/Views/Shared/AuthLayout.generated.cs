﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using MomentumPlus.Relay.Authorization.Models;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Shared/AuthLayout.cshtml")]
    public partial class _Views_Shared_AuthLayout_cshtml : System.Web.Mvc.WebViewPage<dynamic>
    {
        public _Views_Shared_AuthLayout_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 1 "..\..\Views\Shared\AuthLayout.cshtml"
  
    Layout = null;

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<!DOCTYPE html>\r\n\r\n<html");

WriteLiteral(" lang=\"en-gb\"");

WriteLiteral(">\r\n\r\n<head>\r\n    <meta");

WriteLiteral(" charset=\"utf-8\"");

WriteLiteral(" />\r\n    <meta");

WriteLiteral(" http-equiv=\"X-UA-Compatible\"");

WriteLiteral(" content=\"IE=edge\"");

WriteLiteral(" />\r\n    <meta");

WriteLiteral(" name=\"description\"");

WriteLiteral(" content=\"\"");

WriteLiteral(" />\r\n    <meta");

WriteLiteral(" name=\"viewport\"");

WriteLiteral(" content=\"width=device-width, initial-scale=1.0\"");

WriteLiteral(" />\r\n    <link");

WriteLiteral(" rel=\"shortcut icon\"");

WriteAttribute("href", Tuple.Create(" href=\"", 321), Tuple.Create("\"", 357)
            
            #line 14 "..\..\Views\Shared\AuthLayout.cshtml"
, Tuple.Create(Tuple.Create("", 328), Tuple.Create<System.Object, System.Int32>(Url.Content("~/favicon.ico")
            
            #line default
            #line hidden
, 328), false)
);

WriteLiteral(" type=\"image/x-icon\"");

WriteLiteral("/>\r\n\r\n    <title>");

            
            #line 16 "..\..\Views\Shared\AuthLayout.cshtml"
      Write(ViewBag.Title);

            
            #line default
            #line hidden
WriteLiteral("</title>\r\n\r\n    <link");

WriteAttribute("href", Tuple.Create(" href=\"", 430), Tuple.Create("\"", 498)
, Tuple.Create(Tuple.Create("", 437), Tuple.Create<System.Object, System.Int32>(Href("~/Content/lib/webui-popover/dist/jquery.webui-popover.min.css")
, 437), false)
);

WriteLiteral(" rel=\"stylesheet\"");

WriteLiteral(" />\r\n    <link");

WriteAttribute("href", Tuple.Create(" href=\"", 530), Tuple.Create("\"", 575)
, Tuple.Create(Tuple.Create("", 537), Tuple.Create<System.Object, System.Int32>(Href("~/Content/styles/MultiTenancy.Auth.css")
, 537), false)
);

WriteLiteral(" rel=\"stylesheet\"");

WriteLiteral("/>\r\n\r\n    <script");

WriteAttribute("src", Tuple.Create(" src=\"", 610), Tuple.Create("\"", 655)
, Tuple.Create(Tuple.Create("", 616), Tuple.Create<System.Object, System.Int32>(Href("~/Content/lib/jquery/dist/jquery.min.js")
, 616), false)
);

WriteLiteral("></script>\r\n    <script");

WriteAttribute("src", Tuple.Create(" src=\"", 679), Tuple.Create("\"", 744)
, Tuple.Create(Tuple.Create("", 685), Tuple.Create<System.Object, System.Int32>(Href("~/Content/lib/jquery-validation/dist/jquery.validate.min.js")
, 685), false)
);

WriteLiteral("></script>\r\n    <script");

WriteAttribute("src", Tuple.Create(" src=\"", 768), Tuple.Create("\"", 852)
, Tuple.Create(Tuple.Create("", 774), Tuple.Create<System.Object, System.Int32>(Href("~/Content/lib/jquery-validation-unobtrusive/jquery.validate.unobtrusive.min.js")
, 774), false)
);

WriteLiteral("></script>\r\n    <script");

WriteAttribute("src", Tuple.Create(" src=\"", 876), Tuple.Create("\"", 930)
, Tuple.Create(Tuple.Create("", 882), Tuple.Create<System.Object, System.Int32>(Href("~/Content/lib/bootstrap/dist/js/bootstrap.min.js")
, 882), false)
);

WriteLiteral("></script>\r\n    <script");

WriteAttribute("src", Tuple.Create(" src=\"", 954), Tuple.Create("\"", 1020)
, Tuple.Create(Tuple.Create("", 960), Tuple.Create<System.Object, System.Int32>(Href("~/Content/lib/webui-popover/dist/jquery.webui-popover.min.js")
, 960), false)
);

WriteLiteral("></script>\r\n    <script");

WriteAttribute("src", Tuple.Create(" src=\"", 1044), Tuple.Create("\"", 1106)
, Tuple.Create(Tuple.Create("", 1050), Tuple.Create<System.Object, System.Int32>(Href("~/Content/lib/jquery-mask-plugin/dist/jquery.mask.min.js")
, 1050), false)
);

WriteLiteral("></script>\r\n    <script");

WriteAttribute("src", Tuple.Create(" src=\"", 1130), Tuple.Create("\"", 1173)
, Tuple.Create(Tuple.Create("", 1136), Tuple.Create<System.Object, System.Int32>(Href("~/Content/js/relay/sign-in/sign_in.js")
, 1136), false)
);

WriteLiteral("></script>\r\n    <script");

WriteAttribute("src", Tuple.Create(" src=\"", 1197), Tuple.Create("\"", 1249)
, Tuple.Create(Tuple.Create("", 1203), Tuple.Create<System.Object, System.Int32>(Href("~/Content/js/relay/loading-animation.module.js")
, 1203), false)
);

WriteLiteral("></script>\r\n\r\n");

WriteLiteral("    ");

            
            #line 30 "..\..\Views\Shared\AuthLayout.cshtml"
Write(RenderSection("head", required: false));

            
            #line default
            #line hidden
WriteLiteral("\r\n</head>\r\n\r\n<body");

WriteLiteral(" class=\"multi-tenant\"");

WriteLiteral(">\r\n");

WriteLiteral("    ");

            
            #line 34 "..\..\Views\Shared\AuthLayout.cshtml"
Write(Html.Partial("_LoadingAnimation"));

            
            #line default
            #line hidden
WriteLiteral("\r\n    <div");

WriteLiteral(" class=\"multi-tenant-container\"");

WriteLiteral(">\r\n");

WriteLiteral("        ");

            
            #line 36 "..\..\Views\Shared\AuthLayout.cshtml"
   Write(RenderBody());

            
            #line default
            #line hidden
WriteLiteral("\r\n    </div>\r\n");

WriteLiteral("    ");

            
            #line 38 "..\..\Views\Shared\AuthLayout.cshtml"
Write(RenderSection("Scripts", required: false));

            
            #line default
            #line hidden
WriteLiteral("\r\n</body>\r\n\r\n</html>");

        }
    }
}
#pragma warning restore 1591
